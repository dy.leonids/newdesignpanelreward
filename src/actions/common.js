export const requestStarted = ()=>{
  return {
    type : "SET_REQUEST_STARTED"
  }
}

export const requestCompleted = ()=>{
  return {
    type : "SET_REQUEST_COMPLETED"
  }
}
