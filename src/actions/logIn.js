import {submitLoginApi, resetPasswordApi, forgetPasswordMailApi} from "../api/loginApi";
import {requestStarted,requestCompleted} from "./common";

export const setLogOut = () => {
  return{
    type : "LOGOUT"
  }
}

export const setLogin =(authClient) => {
  return {
    type : "SET_AUTH_CLIENT",
    authClient
  }
}
export const setLoginFail =(authFailResponse) => {
  return {
    type : "SET_LOGIN_FAIL",
    authFailResponse
  }
}


export const setMessageFail =() => {
  return {
    type : "SET_MESSAGE_FAIL",
  }
}

export const setResetPassResponse =(response) => {
  return {
    type : "SET_RESET_PASS_MESSAGE",
    response
  }
}

export const resetPasswordMsgFail =(response) => {
  return {
    type : "SET_RESET_PASS_MESSAGE_FAIL",
  }
}

export const setForgetPassMailResponse =(responseMail) => {
  return {
    type : "SET_FORGET_PASS_MAIL",
    responseMail
  }
}

export const setForgetPassFailMailResponse =(responseMail) => {
  return {
    type : "SET_FORGET_PASS_FAIL_MAIL",
    responseMail
  }
}

export const setProfileData =() => {
  return {
    type : "SET_PROFILE_LOGOUT",
    
  }
}


export const logIn = (loginData) => {
  return dispatch => {
    submitLoginApi(loginData).then((response) => {
      if(response.success){
        dispatch(setLogin(response));
      }
      else {
        dispatch(setLoginFail(response));
      }
    });
  }
}

export const submitLogOut = () => {
  return dispatch => {
    submitLoginApi().then((response) => {
    //  if(response.success){
        dispatch(setLogOut(response));
      //}
    });
  }
}


export const resetPassword = (Data) => {
  return dispatch =>{
    dispatch(requestStarted());
    resetPasswordApi(Data).then((response) => {
      dispatch(requestCompleted());
        dispatch(setResetPassResponse(response));
    });
  }
}

export const forgetPasswordMail = (Data) => {
  return dispatch =>{
    dispatch(requestStarted());
    forgetPasswordMailApi(Data).then((responseMail) => {
      dispatch(requestCompleted());
      if(responseMail.success){
        dispatch(setForgetPassMailResponse(responseMail));
      }else{
          dispatch(setForgetPassFailMailResponse(responseMail));
      }
    });
  }
}
