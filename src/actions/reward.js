import { transactionApi, fetchUserTractionApi, getGiftcouponByUserApi} from "../api/rewardApi";
import {studyCountApi} from "../api/studyApi"
import {requestStarted,requestCompleted} from "./common";

export const setTransactionResponse=(response) =>{
  return {
    type: "SET_TRANSACTION_REQUEST_RESPONSE",
    response
  }
}

export const setStudyCountDataReward =(countResponse) => {
  return {
    type : "SET_STUDY_REWARD_COUNT_DATA",
    countResponse
  }
}

export const setUserTransaction = (transactionData) =>{
  return {
    type : "SET_USER_TRANSACTION_DATA",    
    transactionData
  }
}

export const setCouponDetails = (couponData) =>{
  return {
    type : "SET_GIFTCOUPON_ONLINE_DATA",    
    couponData
  }
}

export const transaction = (data) => {
    return dispatch => {
      dispatch(requestStarted());
        transactionApi(data).then((response) => {
          dispatch(requestCompleted());
          dispatch(setTransactionResponse(response));
      });
    }
  }

  

  export const studyCountData=(user) =>{
    return dispatch =>{
      studyCountApi(user).then((result) =>{
        dispatch(setStudyCountDataReward(result));
      });
    }
  }

  export const fetchUserTraction=(user) =>{
    return dispatch =>{
      fetchUserTractionApi(user).then((result) =>{
        dispatch(setUserTransaction(result));
      });
    }
  }

  export const getGiftcouponByUser = (assignId) =>{
    return dispatch =>{
      getGiftcouponByUserApi(assignId).then((result) =>{
        dispatch(setCouponDetails(result));
      });
    }
  }