import { fetchProfileApi , updateProfileApi, addProfileImageApi} from '../api/profileApi';
import {requestStarted,requestCompleted} from "./common"


export const setProfile =(profileData) => {
  return {
    type : "SET_PROFILE",
    profileData
  }
}

export const setprofileUpdateResponse =(updateResponse) =>{
  return {
    type : "SET_PROFILE_UPLOAD_IMAGE_RESPONSE",
    updateResponse
  }
}


export const fetchProfile=(userId , access_token) =>{
  return dispatch =>{
      dispatch(requestStarted());
    fetchProfileApi(userId, access_token).then((result) =>{
       dispatch(requestCompleted());
     dispatch(setProfile(result));
    });
  }
}


export const updateProfile=(userId , access_token, data) =>{
  return dispatch =>{
      dispatch(requestStarted());
    updateProfileApi(userId, access_token, data).then((result) =>{
       dispatch(requestCompleted());
       if(result.success){
         alert(result.message);
         dispatch(setProfile(result));
       }
    });
  }
}

export const addProfileImage=(data, profileID)=>{
  return dispatch =>{
    dispatch(requestStarted());
    addProfileImageApi(data, profileID).then((result) =>{
       dispatch(requestCompleted());
        dispatch(setprofileUpdateResponse(result));
        let access_token=sessionStorage.getItem("token");
        let userId=sessionStorage.getItem("user_id");
        dispatch(fetchProfile(userId, access_token));
    });
  }
}