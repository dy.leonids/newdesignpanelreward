import { fetchCategoryApi, fetchEduQuestionsApi, saveQuestionAnsApi } from '../api/profileDashboardApi';
import {requestStarted,requestCompleted} from "./common"


export const setProfileCategoryDashboard =(categoryData) => {
  return {
    type : "SET_CATEGORY",
    categoryData
  }
}

export const setEduQuestions =(educationData) => {
  return {
    type : "SET_EDUCATION",
    educationData
  }
}

export const fetchCategory=(userId) =>{
  return dispatch =>{
      dispatch(requestStarted());
    fetchCategoryApi(userId).then((result) =>{
       dispatch(requestCompleted());
     dispatch(setProfileCategoryDashboard(result));
    });
  }
}


 export const fetchEduQuestions=(userId, catID) =>{
   return dispatch =>{
       dispatch(requestStarted());
     fetchEduQuestionsApi(userId, catID).then((result) =>{
        dispatch(requestCompleted());
      dispatch(setEduQuestions(result));
     });
   }
 }

export const saveQuestionAns=(userId, catID, quesData) =>{
  return dispatch =>{
      dispatch(requestStarted());
    saveQuestionAnsApi(userId, catID, quesData).then((result) =>{
       dispatch(requestCompleted());
       if(result.questions){
        dispatch(setEduQuestions(result));
       }
       
     //dispatch();
    });
  }
}
