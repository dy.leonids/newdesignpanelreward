import {RegistrationApi, varifyTokenApi}  from "../api/registrationApi";
import {requestStarted,requestCompleted} from "./common";

export const setRegistrationSuccess=(response) =>{
  return {
    type: "SHOW_REGISTER_USER",
    response
  }
}

export const setRegistrationFail=(response) =>{
  return {
    type: "SHOW_CREATE_USER_FAIL_EXCEPTION",
    response
  }
}

export const setRegistrationMessage=() =>{
  return {
    type: "SET_REGISTRATION_MESSAGE"
  }
}

export const varifyTokenResponse=(response) =>{
  return {
    type: "SHOW_TOKEN_VARIFY",
    response
  }
}

export const varifyTokenResponseFail=(responseFail) =>{
  return {
    type: "SHOW_TOKEN_VARIFY_FAIL",
    responseFail
  }
}


export const tokenMsgFail=() =>{
  return {
    type: "SHOW_TOKEN_MSG_FAIL",
  }
}



export const registerUser = (userData)=>{
  return dispatch =>{
    dispatch(requestStarted());
     RegistrationApi(userData).then((response) =>{
         dispatch(requestCompleted());
       if(response.success){
         dispatch(setRegistrationSuccess(response));
         }else{
          dispatch(setRegistrationFail(response));
        }
    });
  }
}

export const varifyToken = (userData)=>{
  return dispatch =>{
    dispatch(requestStarted());
     varifyTokenApi(userData).then((response) =>{
         dispatch(requestCompleted());
       if(response.success){
         dispatch(varifyTokenResponse(response));
         }else{
          dispatch(varifyTokenResponseFail(response));
        }
    });
  }
}
