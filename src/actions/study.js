import { fetchStudyApi , saveStudyParticipateApi, studyParticipateStatusApi, studyCountApi} from '../api/studyApi';
import {requestStarted,requestCompleted} from "./common"


export const setStudy =(studyData) => {
  return {
    type : "SET_STUDY",
    studyData
  }
}

export const setStatusParticipateStudySuccess =(response) => {
  return {
    type : "SET_STUDY_PARTICIPATE_STATUS",
    response
  }
}

export const setStatusParticipateStudyFail =(response) => {
  return {
    type : "FAIL_STUDY_PARTICIPATE_STATUS",
    response
  }
}

export const setStudyCountData =(countResponse) => {
  return {
    type : "SET_STUDY_COUNT_DATA",
    countResponse
  }
}

export const setRewardNull =() =>{
  return{
    type : "SET_REWARD_NULL"
  }
}

export const fetchStudy=(userId , access_token, pageNo) =>{
  return dispatch =>{
    fetchStudyApi(userId, access_token, pageNo).then((result) =>{
     dispatch(setStudy(result));
    });
  }
}

export const saveStudyParticipate=(data, userId) =>{
  return dispatch =>{
    saveStudyParticipateApi(data, userId).then((result) =>{
    //  dispatch(setStudy(result));
    });
  }
}

export const studyParticipate=(data) =>{
  return dispatch =>{
    studyParticipateStatusApi(data).then((result) =>{
      if(result.success){
        dispatch(setStatusParticipateStudySuccess(result));
      }else{
        dispatch(setStatusParticipateStudyFail(result));
      }
    });
  }
}



export const studyCountData=(user) =>{
  return dispatch =>{
    studyCountApi(user).then((result) =>{
      dispatch(setRewardNull());
      dispatch(setStudyCountData(result));
    });
  }
}