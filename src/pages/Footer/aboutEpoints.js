import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import ReactDOM from 'react-dom'

class AboutEpoints extends Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentDidMount(){
    document.title = "Panelreward-AboutEpoints"
  }
  
  
  render() {

   return (
    <div className = "aboutEpointsPage" > 
      <div className = "aboutEpoints"> <h2>About EPoints</h2></div>
      <div className = "aboutEpointsp">
        <p className = "para1">Once registered to PANEL REWARD Surveys, you will be sent surveys for you to respond to. 
        You will accumulate EPoints depending on the contents, number of questions and length of each survey. 
        Accumulated EPoints are redeemable for rewards such as online shopping gift coupons. </p>
        <p className = "para1">50 EPoint =  1 USD .	</p><br/>
        
        <h3 style={{textAlign:"center"}}>About Rewards</h3>
        <h4>Redeem EPoints :</h4>
        <p className = "para1">Currently gift coupons of various amounts are available for you to redeem. The minimum required for redemption is more then 500 EPoints.</p>
        <h5><b>Online Gift Coupon (Starting from Rs. 10)</b></h5><br/>
        
        <h3 style={{textAlign:"center"}}>About Cash Conversion</h3>
        <h4>Transfer to Paypal Account :</h4>
        <p className = "para1">You can transfer your accumulated EPoints to your Paypal account. Minimum required is more then 500 EPoints.</p>
        <p style={{color:"red", textAlign:"left"}}>*Your account must match your registered E-mail address	</p>

        <div class="cashRow" style={{borderStyle: "solid", borderWidth: "1px", borderColor: "#57749D" ,height: "80px",padding:"10px"}}>
          <div class="cashColumn" style={{  width: "30%"  }}>
          <img src={require("./PaypalLogo.png")}  />
          </div>
          <div class="cashColumn"style={{  width: "50%" ,paddingTop:"5px" }} >
          <span>Paytm (Starting from 10 USD)</span><br/>
          <span style={{color:"red"}}>Redemption available from more then 500  EPoints</span>
          </div>
        </div>
      </div>
    </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    
  }
}

export default connect(mapStateToProps)(AboutEpoints);
