import React, { Component } from 'react';
//import './style.css';
import { forgetPasswordMail } from '../../actions/logIn';
import { connect } from 'react-redux';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { Link } from 'react-router-dom';
import panelimg from '../../components/Image/info_graphic_1.svg';
import panelogo from '../../components/Image/panel reward.png';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';

class ForgetPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      notification: "",
      visible: false
    }
    this.onChange = this.onChange.bind(this);
    this.forgetPass = this.forgetPass.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }
  forgetPass() {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = this.state.email;

    let Data = {}
    if (this.state.email === "") {
      this.setState({
        visible: true,
        notification: "Please enter your Email Id"
      });
    } else if (!pattern.test(tempemail)) {
      this.setState({
        visible: true,
        notification: "Please enter correct Email Id"
      });

    } else {
      Data.email = this.state.email;
      this.props.dispatch(forgetPasswordMail(Data));
    }

  }

  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }

  render() {
    let msgData = "";
    let msg = "";
    if (this.props.success === true) {
      if (this.props.resetMessageResponse !== "") {
        msg = this.props.resetMessageResponse;
        msgData = <div className="showTrue" >{msg}
        </div>
      }
    } else {
      if (this.props.resetMessageResponse !== "") {
        msg = this.props.resetMessageResponse;
        msgData = <div className="showFalse" >{msg}
        </div>
      }
    }


    return (

      <div className="wrapper">
        <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="myBtn">&#9776;</button></a>
        <div className="column">
          <div id="icons">

            <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        </div>
          <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
          <div className="subcolumn">
            <div className="image">
              <img src={panelimg} width="400px" />
              <h3 className="heading">Welcome to Panelreward</h3>
              <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
              <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
              <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
              <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to='/register'> <button className="regbtn">Register Now </button></Link>
              <p className="copyright">@ 2018 Panelreward</p>
            </div>
          </div>


        </div>
        <div class="column alt">
          <div id="menubtn">
          </div>
          <div className="form">
            <button className="clickbuttonfrgt">Forgot Password Panelreward</button>
            <p style={{ fontSize: '1.125rem', width: '100%' }}>Enter your email id</p>
            <input type="text" placeholder="Enter E-Mail" class="inputtypefrgt" />
            <button className="clickbuttonfrgt">Send Link</button>
          </div>
          <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn">&times;</button></a>
            <div class="overlay-content">
              <a href="#">About Us</a>
              <a href="#">New Member Registration</a>
              <a href="#">Login</a>
            </div>
          </div>
        </div>
      </div>


      // <div className="main">
      //  <Modal
      //     visible={this.state.visible}
      //     width="400"
      //     height="300"
      //     effect="fadeInDown"
      //     onClickAway={() => this.closeModal()}
      //   >
      //     <div className="text-center" style={{ paddingTop: '20%' }}>
      //       <h1>Alert!</h1>
      //       <p>{this.state.notification}</p>
      //       <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      //     </div>
      //   </Modal>
      //   <div className="middle">
      //     <h1 className="text-center" style={{paddingTop:'6%'}}>Leonids Voice</h1>
      //        <CardGroup style={{paddingTop:'4%',paddingLeft:'25%',paddingRight:'25%',paddingBottom:'4%'}}>
      //       <Card >
      //         <CardBody>
      //           <Form onSubmit={this.login}>
      //             <h3>Forgot Password</h3>
      //             <p className="text-muted text-left" className="mb-4">Please fill your registerd E-mail </p>
      //             <InputGroup  className="mb-4">
      //               <InputGroupAddon addonType="prepend">
      //                 <InputGroupText>
      //                   <img src={email} width="15" height="15" ></img>
      //                 </InputGroupText>
      //               </InputGroupAddon>
      //               <Input type="text" placeholder="Email Id" name="email" value={this.state.email} onChange={this.onChange} />
      //             </InputGroup>
      //                   <Row>
      //                   <Col>
      //                   <div className="loading">{
      //                   this.props.requestStarted ?
      //                     <div className="preloader"><LoadingDots interval={100} dots={5} /></div>
      //                     :
      //                     false
      //                 }</div>
      //                 {msgData}
      //                     <Button style={{ backgroundColor: "#1985AC" }}  onClick={this.forgetPass}>Submit</Button>
      //                   </Col>
      //                 </Row>

      //           </Form>
      //         </CardBody>
      //       </Card>
      //       <Card  className="text-center" style={{ backgroundColor: "#20A8D8" ,color:'white'}}>
      //         <CardBody>
      //           <h1>Login</h1>
      //           <CardText className="text-center" style={{fontSize:'15px'}}>Leonids Voice is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing
      //           research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.
      //           </CardText>
      //           <Link to="/">
      //               <Button className="mt-3" style={{ backgroundColor: "#1985AC" }}>Login Now</Button>
      //             </Link>
      //         </CardBody>
      //       </Card>

      //     </CardGroup>




    );
  }
}

const mapStateToProps = (state) => {
  return {
    resetMessageResponse: state.forgetPassword.message,
    success: state.forgetPassword.success,
    requestStarted: state.common.requestStarted
  }
}

export default connect(mapStateToProps)(ForgetPassword);
