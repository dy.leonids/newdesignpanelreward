import React, { Component } from 'react';
import ProfileDashboardview  from '../../components/ProfileDashboard';
import Education  from '../../components/ProfileDashboard';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { fetchCategory} from '../../actions/profileDashboard';
import {connect} from 'react-redux';

class ProfileDashboard extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    this.catView = this.catView.bind(this);
    this.categoryView = this.categoryView.bind(this);
  }

  componentWillMount() {
    let userId=sessionStorage.getItem("user_id");
        this.props.dispatch(fetchCategory(userId));
      }

  catView(data){
    this.props.history.push("/education" +"/cat:"+data.cat_id + "/catName:" +data.catName);
  }
  
  categoryView(catId, catName){ 
    this.props.history.push("/education" +"/cat:"+catId + "/catName:" +catName);
  }

  render() {
    if(!this.props.authenticated){
      let access_token=sessionStorage.getItem("token");
      let userId=sessionStorage.getItem("user_id");
      if(userId){
           
      }else {
      alert("Please login.");
      this.props.history.push("login");
  }
}

    <Education
    profileCategory = {this.props.profileCategory}
    categoryView = {this.categoryView}
    />

   
    
   let ProfileDashboardviewHtml ="";
     ProfileDashboardviewHtml =   <ProfileDashboardview
                                    profileCategory = {this.props.profileCategory}
                                    requestStarted ={this.props.requestStarted}
                                        catView ={this.catView}
                                    />
   return (<div>
          {ProfileDashboardviewHtml}
           </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
      profileCategory : state.profileDashboard.category,
      requestStarted:state.common.requestStarted,
      authenticated : state.login.authenticated
  }
}

export default connect(mapStateToProps)(ProfileDashboard);
