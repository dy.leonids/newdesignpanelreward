import React, { Component } from 'react';
//import './style.css';
import {Link} from 'react-router-dom';
import { studyParticipate} from '../../actions/study';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import {connect} from 'react-redux';


class StudyParticipate extends Component {
  constructor(props){
    super(props)

    this.state={

    }

  }





  componentDidMount(){
    let studyId = ( window.location.pathname.split("/")[2].split("s:")[1]);
    let userId = ( window.location.pathname.split("/")[3].split("u:")[1]);
    let statusId = ( window.location.pathname.split("/")[4].split("status:")[1]);
    let Data = {}
      Data.studyId = studyId;
      Data.userId = userId;
      Data.status =  statusId;
     this.props.dispatch(studyParticipate(Data));
  }


  render() {

    let msg = "";
    let msgData = "";

    if(this.props.success === true ){
      msg = this.props.message;
        msgData = <div>
                  {msg}
                  </div>
      }else{
        if(this.props.message){
          msg = this.props.message;
            msgData = <div className = "tPassMsgErr">{msg}</div>
        }
      }




    return (
      <div className = "studyParticipateView">
            <div className="loading">{
                 this.props.requestStarted?
                 <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
                 :
                 false
               }</div><br/>
               {msgData}


      </div>
    );
  }
}

const  mapStateToProps =(state) =>{
  return {
    message : state.study.messages,
    success : state.study.success
  }
}

export default  connect(mapStateToProps)(StudyParticipate);
