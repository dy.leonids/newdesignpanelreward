import React, { Component } from 'react';
//import './style.css';
import {connect} from 'react-redux';
import appConfig from '../../api/apiConfig';


class CompaneyParticipated extends Component {
  constructor(props){
    super(props)

    this.state={
      status:"",
      buttonShow: true
    }
    this.onChange = this.onChange.bind(this);
    this.studyParticipateStatus = this.studyParticipateStatus.bind(this);
  }

  onChange(e){
  this.setState({[e.target.name]: e.target.value});
  }

  componentDidMount(){
    document.title = "Panelreward-participant"
  }

  studyParticipateStatus(){
    let url ="";
    let statusId =  this.state.status;
    let studyAndUserId = window.location.search.split("&UID")[1];
    let userId = ( window.location.search.split("&UID")[1].split("$SID$")[1].split("@UID@")[1]);

      

    if(appConfig.url.includes("localhost:")){
      // let url ="http://localhost:4010/studyParticipate/s:"+ studyId + "/u:"+ userId+"/status:" + statusId ;
      // let url = appConfig.url +"studyParticipate/s:"+ studyId + "/u:"+ userId+"/status:" + statusId ;
      // window.open(url,'_self'); 

      if(statusId == 1){   //"Completed"
      url ="http://localhost:4011/studyParticipateCompleted/UID"+studyAndUserId;
     
      window.open(url,'_self'); 

      }else if(statusId == 2){   //"Quota Fail"
      url ="http://localhost:4011/studyParticipateQuotaFail/UID"+studyAndUserId;
     
      window.open(url,'_self'); 
      }else if(statusId == 3){    //Fail
        url ="http://localhost:4011/studyParticipateFail/UID"+studyAndUserId;
     
      window.open(url,'_self'); 
     }
    }else{
      // server
      if(statusId == 1){   //"Completed"
      url = appConfig.completedUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 

      }else if(statusId == 2){   //"Quota Fail"
      url = appConfig.quotaFailUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 

      }else if(statusId == 3){    //Fail
      url = appConfig.failUrl+"UID"+studyAndUserId;
      window.open(url,'_self'); 
      }
    }
  }




  render() {

   
    return (
      <div className = "studyParticipateView">
      <p className = "para" style = {{ color: "#fff"}}> Select status  </p>
      <label style = {{ color: "#fff"}}>Please select status for participated Study</label><br/>
      <input type="radio" name="status" value="1"  onChange={this.onChange}  /><label style = {{ color: "#fff"}}> Completed</label><br/>
      <input type="radio" name="status" value="2"  onChange={this.onChange} /><label style = {{ color: "#fff"}}> QuotaFail</label><br/>
      <input type="radio" name="status" value="3"  onChange={this.onChange} /><label style = {{ color: "#fff"}}> Fail </label><br/><br/>
      <button  className="studyParticipatebtn" onClick={this.studyParticipateStatus} >Submit</button><br/>
      </div>
    );
  }


}

export default  CompaneyParticipated;
