import React, { Component } from 'react';
import {connect} from 'react-redux';
import {studyCountData} from '../../actions/study';
import {Link} from 'react-router-dom';
import './styleCount.css';


class StudyCount extends Component {
  constructor(props){
    super(props)

    this.state={
      totalStudy :"",
      startStudy   :"",
      completeStudy :"",
      totalRewards : "",
      failStudy: "",
      avalibleStudy:"",
      showButton: true,
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(e){
  this.setState({[e.target.name]: e.target.value});
  }

  componentDidMount(){
    document.title = "Panelreward-Dashboard"
  }

  componentWillMount(){
    let userId=sessionStorage.getItem("user_id");
    this.props.dispatch(studyCountData(userId));
  }

  componentWillReceiveProps(Data){

      this.setState({
        totalStudy : Data.StudyCountData.totalStudy,
        startStudy  : Data.StudyCountData.startStudy,
        completeStudy : Data.StudyCountData.completeStudy,
        totalRewards : Data.StudyCountData.totalRewards,
        failStudy : Data.StudyCountData.failStudy,
        avalibleStudy : parseInt(Data.StudyCountData.totalStudy) - parseInt(Data.StudyCountData.failStudy)
        -parseInt(Data.StudyCountData.completeStudy)-parseInt(Data.StudyCountData.quotaFailStudy)-parseInt(Data.StudyCountData.startStudy),
      });
  }

  render() {
    if(!this.props.authenticated){
      let access_token=sessionStorage.getItem("token");
      let userId=sessionStorage.getItem("user_id");
      if(userId){
           
        }else {
        alert("Please login.");
        this.props.history.push("login");
      }
    }
    let studyView ="";
    let redeemButton = "";

    if(this.state.totalRewards > 500){
      redeemButton = <button ><Link to="/collectReward" className="collect">Redeem</Link> </button>
    }

      if(this.props.success){
    studyView = <table className = "countTable">
        <tr>
          <th className = "countTh">TotalStudy</th>
          <td>{this.state.totalStudy}</td><td></td>
        </tr>
        <tr>
          <th className = "countTh">AvalibleStudy</th>
          <td>{this.state.avalibleStudy}</td><td></td>
        </tr>
        <tr>
          <th className = "countTh">StartStudy</th>
          <td>{this.state.startStudy}</td><td></td>
        </tr>
        <tr>
          <th className = "countTh">CompleteStudy</th>
          <td>{this.state.completeStudy}</td><td></td>
        </tr>
        <tr>
          <th className = "countTh">FailStudy</th>
          <td>{this.state.failStudy}</td><td></td>
        </tr>
        <tr>
          <th className = "countTh">Total number of rewards earned</th>
          <td>{this.state.totalRewards} </td>
          <td>{redeemButton}</td>
        </tr>
        

    </table>
  }


    return (
      
      <div className = "countForm">
      <div className = "countback">
      <div className = "headings"><h1>Dashboard</h1></div>
      <div className = "tablecount">
     
      {studyView}</div>
      </div>
      </div>
      
    );
  }
}

const  mapStateToProps =(state) =>{
  return {
    StudyCountData : state.study.StudyCountData,
    success : state.study.success
  }
}

export default  connect(mapStateToProps)(StudyCount);
