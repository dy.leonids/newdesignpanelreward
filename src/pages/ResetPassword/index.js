import React, { Component } from 'react';
//import './style.css';
import { Link } from 'react-router-dom';
import { resetPassword, resetPasswordMsgFail } from '../../actions/logIn';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { connect } from 'react-redux';
import panelimg from '../../components/Image/info_graphic_1.svg';
import panelogo from '../../components/Image/panel reward.png';
import facebook from '../../components/Image/facebook-logo.svg';
import google from '../../components/Image/google-logo.svg';
import twitter from '../../components/Image/twitter.svg';
import linked from '../../components/Image/linkedin.svg';

class RestPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      visible: false,
      notification: ""
    };
    this.onChange = this.onChange.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
  }
  closeModal() {
    this.setState({
      visible: false
    });
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  resetPassword() {
    this.props.dispatch(resetPasswordMsgFail());

    let Data = {}

    if (this.state.password === "") {
      this.setState({
        visible: true,
        notification: "Please enter password"
      });
      //alert('Please enter password');
    } else if (this.state.confirmPassword === "") {
      this.setState({
        visible: true,
        notification: "Please confirm password"
      });
      //alert('Please enter confirm password');
    } else {
      if (this.state.password === this.state.confirmPassword) {
        if (window.location.pathname.split('sn:')[1]) {
          Data.id = window.location.pathname.split('sn:')[1];
          Data.password = this.state.password;
          this.props.dispatch(resetPassword(Data));
        } else {
          this.setState({
            visible: true,
            notification: "You have not authority to change password Please retry."
          });
          //alert('You have not authority to change password Please retry.');
        }

      } else {
        this.setState({
          visible: true,
          notification: "Password didn't match"
        });
        // alert('Password did not match')
      }

    }

  }

  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }

  render() {
    let msg = "";
    let msgData = "";
    if (this.props.success === true) {
      this.setState({
        visible: true,
        notification: this.props.resetMessageResponse
      });
      //alert(this.props.resetMessageResponse);
      this.props.history.push("/login");
      // msg = this.props.resetMessageResponse;
      //   msgData = <div className = "fPassMsgSucc">
      //             {msg}
      //             <Link to="/login"> Sign in </Link>
      //             </div>
    } else {
      if (this.props.resetMessageResponse) {
        msg = this.props.resetMessageResponse;
        msgData = <div className="fPassMsgErr">{msg}</div>
      }
    }


    return (
      <div className="wrapper">
        <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="myBtn">&#9776;</button></a>
        <div className="column">
          <div id="icons">

            <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
    <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
    <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
    <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
      </div>
          <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
          <div className="subcolumn">
            <div className="image">
              <img src={panelimg} width="400px" />
              <h3 className="heading">Welcome to Panelreward</h3>
              <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
              <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
              <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
              <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to='/register'> <button className="regbtn">Register Now </button></Link>
              <p className="copyright">@ 2018 Panelreward</p>
            </div>
          </div>
        </div>
        <div class="column alt">
          <div id="menubtn">
          </div>
          <div className="form">
            <button className="clickbuttonfrgt">Reset Password Panelreward</button>
            <input type="text" placeholder="Password" class="inputtypereset" />
            <input type="text" placeholder="Confirm Password" class="inputtypereset" />
            <button className="clickbuttonreset">Submit</button>
          </div>
          <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn">&times;</button></a>
            <div class="overlay-content">
              <a href="#">About Us</a>
              <a href="#">New Member Registration</a>
              <a href="#">Login</a>
            </div>
          </div>
        </div>
      </div>





      //       <div className="reset">
      //           <Modal
      //           visible={this.state.visible}
      //           width="400"
      //           height="300"
      //           effect="fadeInDown"
      //           onClickAway={() => this.closeModal()}
      //         >
      //           <div className="text-center" style={{ paddingTop: '20%' }}>
      //             <h1>Alert!</h1>
      //             <p>{this.state.notification}</p>
      //             <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      //           </div>
      //         </Modal>
      //         <h1 className="text-center" style={{ paddingTop: '6%' }}>Leonids Voice</h1>
      //         <CardGroup style={{ paddingTop: '4%', paddingLeft: '25%', paddingRight: '25%', paddingBottom: '4%' }}>
      //           <Card >
      //             <CardBody>
      //               <Form onSubmit={this.login}>
      //                 <h3>Reset Password</h3>
      //                 <p className="text-muted text-left">Please fill this form to reset an user</p>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={lock} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="password" placeholder="Enter Password" name="password" value={this.state.password} onChange={this.onChange} />
      //                 </InputGroup>
      //                 <InputGroup className="mb-3">
      //                   <InputGroupAddon addonType="prepend">
      //                     <InputGroupText>
      //                       <img src={lock} width="15" height="15"></img>
      //                     </InputGroupText>
      //                   </InputGroupAddon>
      //                   <Input type="password" placeholder="Enter Confirm Password" name="confirmPassword" value={this.state.confirmPassword} onChange={this.onChange} />
      //                 </InputGroup>
      //                 <Row>
      //                   <Col>
      //                     <Button style={{ backgroundColor: "#1985AC" }} style={{ backgroundColor: "#1985AC" }} onClick={this.resetPassword}>Submit</Button>
      //                   </Col>
      //                 </Row>
      //                 <div className="loading">{
      //                   this.props.requestStarted ?
      //                     <div className="preloader"><LoadingDots interval={100} dots={5} /></div>
      //                     :
      //                     false
      //                 }</div>
      //                 {msgData}
      //               </Form>
      //             </CardBody>
      //           </Card>
      //           <Card className="text-center" style={{ backgroundColor: "#20A8D8", color: 'white' }}>
      //             <CardBody>
      //               <h3>Login</h3>
      //               <CardText className="text-center" style={{ fontSize: '15px' }}>Leonids Voice is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing
      //               research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.
      //                 </CardText>
      //               <Link to="/">
      //                 <Button className="mt-3" style={{ backgroundColor: "#1985AC" }}>Login Now</Button>
      //               </Link>
      //             </CardBody>
      //           </Card>

      //         </CardGroup>
      // </div>


    );
  }
};

const mapStateToProps = (state) => {
  return {
    resetMessageResponse: state.resetPassword.message,
    success: state.resetPassword.success,
    requestStarted: state.common.requestStarted
  }
}

export default connect(mapStateToProps)(RestPassword);
