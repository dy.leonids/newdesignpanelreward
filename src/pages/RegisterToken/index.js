import React, { Component } from 'react';
//import './style.css';
import {Link} from 'react-router-dom';
import { varifyToken, tokenMsgFail} from '../../actions/registration';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import {connect} from 'react-redux';


class RegisterToken extends Component {
  constructor(props){
    super(props)

    this.state={
      token:"",
    }
    this.onChange = this.onChange.bind(this);
    this.varifyToken = this.varifyToken.bind(this);
  }

  onChange(e){
  this.setState({[e.target.name]: e.target.value});
  }


  varifyToken(){
     this.props.dispatch(tokenMsgFail());
    let Data = {}
    let userId = window.location.pathname.split("/");
      Data.id = userId[2];
      Data.token =  this.state.token;
      this.props.dispatch(varifyToken(Data));
  }


  render() {
    let msg = "";
    let msgData = "";
    if(this.props.success === true ){
      msg = this.props.resetMessageResponse;
        msgData = <div className = "responsMsgToken">
                  {msg}
                  </div>
      }else{
        if(this.props.resetMessageResponse){
          msg = this.props.resetMessageResponse;
            msgData = <div className = "tPassMsgErr">{msg}</div>
        }
      }


    return (
      <div >
          <div className = "loginFormDiv">
              <div className= "loginFormHearder"><h1> Varify Token </h1>
              
              </div>
              <p>Please fill in this form to varify an user.</p>

              <div className= "loginForm">
                <input type="text" className = "registerTokenInput" placeholder="Enter Token" name="token" value={this.state.token} onChange={this.onChange} /><br/>
                <button  className="registerTokenBtn" onClick={this.varifyToken} >Submit</button><br/>
              <br/>
              <div className="loading">{
                   this.props.requestStarted?   
                   <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
                   :
                   false
                 }</div>

              {msgData}
              </div>
            </div>
      </div>
    );
  }
}

const  mapStateToProps =(state) =>{
  return {
  resetMessageResponse : state.registration.message,
  success : state.registration.success,
  requestStarted:state.common.requestStarted
  }
}

export default  connect(mapStateToProps)(RegisterToken);
