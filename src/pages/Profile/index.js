import React, { Component } from 'react';
import ProfileView  from '../../components/Profile';
import {fetchProfile, updateProfile, addProfileImage} from '../../actions/profile';
import {connect} from 'react-redux';

class Profile extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    this.updateProfile = this.updateProfile.bind(this);
    this.addProfileImage = this.addProfileImage.bind(this);
  }

  componentWillMount() {
    let access_token=sessionStorage.getItem("token");
    let userId=sessionStorage.getItem("user_id");
     if(userId){
      this.props.dispatch(fetchProfile(userId, access_token));
    }
  }

  updateProfile(data){
    let access_token=sessionStorage.getItem("token");
       let userId=sessionStorage.getItem("user_id");
        if(userId){
         this.props.dispatch(updateProfile(userId, access_token, data));
       }
  }

  addProfileImage(data, profileID){
    this.props.dispatch(addProfileImage(data, profileID));
  }

  render() {
    if(!this.props.authenticated){
      let access_token=sessionStorage.getItem("token");
      let userId=sessionStorage.getItem("user_id");
      if(userId){
    //   this.props.dispatch(fetchStudy(userId, access_token));
      }else {
        alert("Please login.");
        this.props.history.push("login");
      }
  }

   let profileView ="";
    if(this.props.userProfile){
      profileView =   <ProfileView
                       userProfile = {this.props.userProfile}
                       updateProfile = {this.updateProfile}
                       addProfileImage = {this.addProfileImage}
                       profileImageUploadRes = {this.props.profileImageUploadRes}
                         />
    }

   return (<div>
          {profileView}
           </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated : state.login.authenticated,
    userProfile : state.profile.userProfile,
    profileImageUploadRes : state.profile.profileImageUploadRes
  }
}

export default connect(mapStateToProps)(Profile);
