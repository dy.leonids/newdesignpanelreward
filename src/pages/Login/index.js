import React, { Component } from 'react';
import './style.css';
import { logIn, setMessageFail } from '../../actions/logIn';
import { setRegistrationMessage } from '../../actions/registration';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import FacebookLoginButton from 'react-facebook-login';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import panelimg from './info_graphic_1.svg';
import panelogo from './panel reward.png';
import facebook from './facebook-logo.svg';
import google from './google-logo.svg';
import twitter from './twitter.svg';
import linked from './linkedin.svg';

class LogIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //isLoggedIn: false,
      userID: '',
      name: '',
      picture: '',
      email: "",
      password: "",
      errorMessage: "",
      visible: false,
      notification: ""
    }
    this.emailChange = this.emailChange.bind(this);
    this.passChange=this.passChange.bind(this);
    this.login = this.login.bind(this);
    this.setMessage = this.setMessage.bind(this);

  }
  emailChange(e){
    let valid=true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = this.state.email;
    if(e.target.value===""){
      document.getElementById("error").style.display="block";
    }else{
      document.getElementById("error").style.display="none";
      
    }
    this.setState({email:e.target.value});
  }
  passChange(e){
    if(e.target.value===""){
      document.getElementById("error1").style.display="block";
    }else{
      document.getElementById("error1").style.display="none";
    }
    this.setState({password:e.target.value});
  }
  // componentWillMount() {
     
  //   if (!this.props.authenticated) {
  //     let access_token = sessionStorage.getItem("token");
  //     let userId = sessionStorage.getItem("user_id");
  //     if (userId) {
  //       this.props.history.push("/studyCount");
  //     }
  //   }
  // }

  setMessage() {
    this.props.dispatch(setRegistrationMessage());
  }


  // onChange(e) {
  //   if(e.target.name==="email" & e.target.value===""){
  //       document.getElementById("error").style.display="block";
  //       //alert("enter email");
  //   }
  //   else{
  //     document.getElementById("error").style.display="none";
      
  //   }
  //   if(e.target.name==="password" & e.target.value===""){
  //     document.getElementById("error1").style.display="block";
  //     // alert("enter pass");
  //   }else{
  //     document.getElementById("error1").style.display="none";
      
  //   }
  //   this.setState({ [e.target.name]: e.target.value });
  // }

  handleValidation() {
    let valid=true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    let tempemail = this.state.email;
    if(this.state.email===""){
      valid=false;
      document.getElementById("error").style.display="block";
      //alert("Enter email");
      return valid;

    }
    if(this.state.password===""){
      valid=false;
      document.getElementById("error1").style.display="block";
      //alert("Enter pass");
      return valid;
    }
   
    return valid;


    // let valid = true;
    // var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    // let tempemail = this.state.email;
    //  if (this.state.email === "") {
    //   valid = false;
    //   //document.getElementById("error").style.display="block";
    //   alert("Please enter Email Id");
    
    //   return valid;
    // }
    // else if (!pattern.test(tempemail)) {
    //   valid = false;
    //   this.setState({
    //     visible: true,
    //     notification: "Please enter correct Email Id"
    //   });
    //   return valid;
    // }
    // if(this.state.password === ""){
    //   valid=false;
    //   alert("enter password");
    //   return valid;
    // }
    // return valid;
  }

  closeModal() {
    this.setState({
        visible: false
    });
}

  login(e) {
    e.preventDefault();
    let logInData = {}
    if (this.handleValidation()) {
      logInData.email = this.state.email;
      logInData.password = this.state.password;
      logInData.registerType = "email";
      this.props.dispatch(logIn(logInData));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.authenticated) {
      this.props.history.push('/studyCount');
    } else if (nextProps.logInFailMessage != null && nextProps.logInFailMessage != "") {
      this.setState({ errorMessage: nextProps.logInFailMessage });
      this.props.dispatch(setMessageFail());
    }
    else {
      this.setState({ errorMessage: "" });
    }
  }

  responseFacebook = (response) => {
    console.log(response.name + " " + response.email + " " + response.picture.data.url + "  ,id: " + response.id);
    //this.setState({isLoggedIn : false});


    let logInData = {}
    logInData.registerType = "facebook";
    logInData.facebookID = response.id;
    logInData.firstName = response.name;
    logInData.lastName = "";
    logInData.email = response.email;
    logInData.imageURL = response.picture.data.url;
    logInData.Token = response.Token;
    this.props.dispatch(logIn(logInData));
  }


  componentClicked = () => console.log("clicked");
 
  openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("myBtn").style.display = "none";
  }
  closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myBtn").style.display = "block";
  }



  render() {
    
    if (this.state.errorMessage !== "") {

      this.setState({
        visible: true,
        notification: this.state.errorMessage
      });
      // alert(this.state.errorMessage);
    }
    // ☰  
    return (
     
        <div className="wrapper">
          <a href="javascript:void(0)" onClick={() => this.openNav()}> <button id="myBtn">&#9776;</button></a>
          <div className="column">
            <div id="icons">

            <img src={facebook} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={google} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={twitter} height="18px" width="18px" className="link" />&nbsp;&nbsp;
        <img src={linked} height="18px" width="18px" className="link" />&nbsp;&nbsp;
          </div>
            <div className="logo"><img src={panelogo} height="50px" height="50px" /></div>
            <div className="subcolumn">
              <div className="image">
                <img src={panelimg} width="400px" />
                <h3 className="heading">Welcome to Panelreward</h3>
                <p className="title" style={{ fontWeight: '700' }}>Data Collection service</p>
                <p className="paragraph">GNN is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.</p>
                <p className="title" style={{ fontWeight: '700' }}>Data Analysis service</p>
                <p className="paragraph">We provide advanced analytics services to help companies drill deeper into the data and understand the subterranean data linkages which drive customer behavior. Our Analytics services help companies to take data driven decisions and hone their marketing programs.</p>
              <Link to="/register"><button className="regbtn">Register Now</button></Link>
                <p className="copyright">@ 2018 Panelreward</p>
              </div>
            </div>


          </div>
          <div class="column alt">
            <div id="menubtn"> </div>
            <div className="form">
              <button className="clickbutton">Login into Panelreward</button>
              <p style={{ fontSize: '1.125rem', width: '100%' }}>Login into your account</p>
              <div style={{ position: 'relative' }}>
                <input type="text" placeholder="Enter E-Mail" class="inputtype" name="email" onChange={this.emailChange} />
                <div id="error">
                            <div class="quote">
                                    <blockquote>
                                       Required
                                        </blockquote>
                                    </div>
                            </div>
                            <div id="mailerror">
                            <div class="mailquote">
                                    <blockquote>
                                       Incorrect
                                        </blockquote>
                                    </div>
                            </div>
              </div>
              <div style={{ position: 'relative' }}>
              <input type="password" placeholder="Enter Password" class="inputtype" name="password" onChange={this.passChange} />
              <div id="error1">
                            <div class="quote">
                                    <blockquote>
                                       Required
                                        </blockquote>
                                    </div>
                            </div>
              </div>
              <button className="clickbutton" onClick={this.login}>Login</button>
              <Link to='/forgetpassword'> <button className="frgtbtn">Forgot Password</button></Link>
            </div>
            <div id="myNav" class="overlay">
              <a href="javascript:void(0)" class="closebtn" onClick={() => this.closeNav()}><button id="myclsbtn">&times;</button></a>
              <div class="overlay-content">
                <a href="#">About Us</a>
                <a href="#">New Member Registration</a>
                <a href="#">Login</a>
              </div>
            </div>
          </div>
        </div>
        
     






      // <div className="app">

      //   <Modal
      //     visible={this.state.visible}
      //     width="400"
      //     height="300"
      //     effect="fadeInDown"
      //     onClickAway={() => this.closeModal()}
      //   >
      //     <div className="text-center" style={{ paddingTop: '20%' }}>
      //       <h1>Alert!</h1>
      //       <p>{this.state.notification}</p>
      //       <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      //     </div>
      //   </Modal>


      //   {/* <h1 className="text-center" style={{ paddingTop: '6%' }}>Leonids Voice</h1> */}
      //     <div>
      //        <div id="title"><h1> Leonids Voice</h1></div>
      //       <div id="elecpoll">
      //       <h4>Election Poll Survey</h4><br/>
      //       <Link to='//survey.leonids.in' target="_blank"><button id="india">INDIA 2019</button></Link>
      //      <br/><br/>
      //      <Link to='//survey.leonids.in' target="_blank"><button id="usa">USA 2020</button></Link>

      //       </div>
      //     </div>

      //   <div>
      //     <CardGroup style={{ paddingTop: '4%', paddingLeft: '25%', paddingRight: '25%', paddingBottom: '4%' }}>
      // <Card >
      //   <CardBody>
      //     <Form onSubmit={this.login}>
      //       <h3>Login</h3>
      //       <p className="text-muted text-left">Sign in to your account</p>
      //       <InputGroup className="mb-3">
      //         <InputGroupAddon addonType="prepend">
      //           <InputGroupText>
      //             <img src={email} width="15" height="15" ></img>
      //           </InputGroupText>
      //         </InputGroupAddon>
      //         <Input type="text" placeholder="Email Id" name="email" value={this.state.email} onChange={this.onChange} />
      //       </InputGroup>
      //       <InputGroup className="mb-3">
      //         <InputGroupAddon addonType="prepend">
      //           <InputGroupText>
      //             <img src={lock} width="15" height="15"></img>
      //           </InputGroupText>
      //         </InputGroupAddon>
      //         <Input type="password" placeholder="Password" name="password" value={this.state.password} onChange={this.onChange} />
      //       </InputGroup>

      //       <div className="loading">{
      //         this.props.requestStarted ?
      //           <div className="preloader"><LoadingDots interval={100} dots={5} /></div>
      //           :
      //           false
      //       }</div>
      //       <Row className="logforget">
      //         <Col>
      //           <Button style={{ backgroundColor: "#1985AC" }}>Login</Button>
      //         </Col>
      //         <Col>
      //           <Link to="/forgetPassword" className="forgetPass">Forgot Password ? </Link>
      //         </Col>
      //       </Row>

      //     </Form>
      //   </CardBody>
      // </Card>
      //       <Card className="text-center" style={{ backgroundColor: "#20A8D8", color: 'white' }}>
      //         <CardBody>
      //           <h3>Sign Up</h3>
      //           <CardText className="text-justify" style={{ fontSize: '15px' }}>Leonids Voice is a leading service provider in the field of conducting professional market research services be it quantitative & qualitative. We hold expertise in providing both primary and secondary research based services. It helps us to provide improvised marketing
      //           research study designs & analytical expertise that help us in successfully meeting client’s customized research needs.
      //           </CardText>
      //           <Link to="/Register">
      //             <Button className="mt-3" style={{ backgroundColor: "#1985AC" }}>Register Now</Button>
      //           </Link>
      //         </CardBody>
      //       </Card>

      //     </CardGroup>
      //   </div>
      // </div>



    );
  }
}


const mapStateToProps = (state) => {
  return {
    status: state.login.success,
    authenticated: state.login.authenticated,
    logInFailMessage: state.login.message,
  }
}

export default connect(mapStateToProps)(LogIn);
