import appConfig from './apiConfig';

export const fetchCategoryApi =(userId)=>{

const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                           });
const request= new Request( appConfig.siteURL+'getAllProfileCategory/'+ userId, {
  method : "GET",
  headers : headers,
});

return fetch(request).then(response => {
    return  response.json().then(categoryDataResponse => {
      return categoryDataResponse;
    });
  }).catch(error => {
   return error;
 });
}


export const fetchEduQuestionsApi =(userId, catID)=>{

const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                           });
const request= new Request( appConfig.siteURL+'getQuestionDataByuser/'+catID+'/'+userId, {
  method : "GET",
  headers : headers,
});

return fetch(request).then(response => {
    return  response.json().then(questionsDataResponse => {
      return questionsDataResponse;
    });
  }).catch(error => {
   return error;
 });
}


//saveQuestionAnsApi
export const saveQuestionAnsApi =(userId, catID, quesData)=>{
let quensData =  JSON.stringify(quesData);
const headers = Object.assign({'content-type':'application/json',
                               'accept':'*/*'
                           });
const request= new Request( appConfig.siteURL+'insertUserAns/'+catID+'/'+userId, {
  method : "POST",
  headers : headers,
  body : quensData,
});

return fetch(request).then(response => {
    return  response.json().then(saveQuesAnsResponse => {
      return saveQuesAnsResponse;
    });
  }).catch(error => {
   return error;
 });
}
