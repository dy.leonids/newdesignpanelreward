import appConfig from './apiConfig';

export const transactionApi =(Data)=>{
let objLogin =  JSON.stringify(Data);

const headers = Object.assign({'content-type':"application/json",
                               'accept':'*/*'});
const request= new Request( appConfig.siteURL+'/addTransaction', {

//const request= new Request( app.siteURL + '/api/login', {
  method : "POST",
  headers : headers,
  body : objLogin
});

return fetch(request).then(response => {
    return  response.json().then(transactionResponse => {

      return transactionResponse;
    });
  }).catch(error => {
   return error;
 });
}


//fetchUserTractionApi
export const fetchUserTractionApi =(Data)=>{

const headers = Object.assign({'content-type':"application/json",
                               'accept':'*/*'});
const request= new Request( appConfig.siteURL+'/fetchUserTraction/'+Data, {

//const request= new Request( app.siteURL + '/api/login', {
  method : "GET",
  headers : headers

});

return fetch(request).then(response => {
    return  response.json().then(transactionResponse => {

      return transactionResponse;
    });
  }).catch(error => {
   return error;
 });
}

//getGiftcouponByUserApi
export const getGiftcouponByUserApi =(assignId)=>{

  const headers = Object.assign({'content-type':"application/json",
                                 'accept':'*/*'});
  const request= new Request( appConfig.siteURL+'/getGiftcouponByUser/'+assignId, {
  
  //const request= new Request( app.siteURL + '/api/login', {
    method : "GET",
    headers : headers
  
  });
  
  return fetch(request).then(response => {
      return  response.json().then(transactionResponse => {
  
        return transactionResponse;
      });
    }).catch(error => {
     return error;
   });
  }