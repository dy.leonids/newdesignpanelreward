import appConfig from './apiConfig';

export const RegistrationApi =(registrationData)=>{

let objLogin =  JSON.stringify(registrationData);

const headers = Object.assign({'Content-type':'application/json',
                              });
const request= new Request( appConfig.siteURL+'register-user/', {
  method : "POST",
  headers : headers,
  body : objLogin
});
  return  fetch(request).then(response => {
    return  response.json().then(registrationResponse => {
      return registrationResponse;
    });
  }).catch(error => { return error; });
}

//---------------------//
//--- varifyTokenApi--//
//---------------------//
export const varifyTokenApi =(userData)=>{

let objUser =  JSON.stringify(userData);

const headers = Object.assign({'Content-type':'application/json',
                              });
const request= new Request( appConfig.siteURL+'varifyToken/', {
  method : "POST",
  headers : headers,
  body : objUser
});
  return  fetch(request).then(response => {
    return  response.json().then(registrationResponse => {
      return registrationResponse;
    });
  }).catch(error => { return error; });
}
