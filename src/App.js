
import React, { Component } from 'react';
import { BrowserRouter as Router , Route, Switch } from 'react-router-dom';
import Menu from './containers/Menu';
import Logout from './containers/Logout';
import Registers from './pages/Register';
import Study from './pages/Study';
import LogIn from './pages/Login';
import ResetPassword from './pages/ResetPassword'
import ForgetPassword from './pages/ForgetPassword'
import RegisterToken from './pages/RegisterToken'
import Profile from './pages/Profile'
import ProfileDashboard from './pages/ProfileDashboard'
import Education from './components/ProfileDashboard/education'
import StudyCount from './pages/Study/studyCount'
import StudyParticipate  from './pages/StudyParticipate'
import CompaneyParticipated  from './pages/StudyParticipate/companeyParticipated'
import ThankuPage from './pages/ThankuPage';
import CollectReward from './pages/CollectReward';
import TransactionHistory from './pages/TransactionHistory';

import StudyParticipateComplete from './pages/StudyParticipate/studyParticipateComplete';
import StudyParticipateFail from './pages/StudyParticipate/studyParticipateFail';
import StudyParticipateQuotaFail from './pages/StudyParticipate/studyParticipateQuotaFail';

import Footer from './pages/Footer';
import AboutUs from './pages/Footer/aboutUs';
import TermAndCondition from './pages/Footer/termCondition';
import PrivacyPolicy from './pages/Footer/privacyPolicy';
import AboutEpoints from './pages/Footer/aboutEpoints';

import Home from './pages/Home';
class App extends Component {
  render() {
    return (
      <div>
      <div className = "App">
        <header  className="header">
        {/* <img src={require("./gnnOpinionImg.png")}  className = "appimage"/><br/>
        <div className = "horizental"></div> */}
        </header>
      <Router>
          <div >
            <div >
              <Menu />
            </div>
            <div >
              <Switch>
                <Route component={Home} exact path="/" />
                <Route component={Registers} exact path="/register" />
                <Route component={LogIn} exact path="/Login" />
                <Route component={Study} exact path="/Study" />
                <Route component={Profile} exact path="/Profile" />
                <Route component={Logout} path="/logout" />
                <Route component={ResetPassword} path="/resetPassword" />
                <Route component={ForgetPassword} path="/forgetPassword" />
                <Route component={RegisterToken} path="/registerToken/:key" />
                <Route component={StudyParticipate} path="/studyParticipate/:key" />
                <Route component={CompaneyParticipated} path="/companeyParticipated/:key?" />
                <Route component={StudyCount} path="/dashboard" />
                <Route component={ProfileDashboard} path="/profileDashboard"/>
                <Route component={Education} path="/education/:key"/>
                <Route component={Home} exact path="/home" />
                <Route component={ThankuPage} exact path="/thankuPage" />
                <Route component={CollectReward} exact path="/collectReward" />
                <Route component={TransactionHistory} exact path="/transactionHistory" />

                <Route component={StudyParticipateComplete} exact path="/studyParticipateCompleted/:key" />
                <Route component={StudyParticipateFail} exact path="/studyParticipateFail/:key" />
                <Route component={StudyParticipateQuotaFail} exact path="/studyParticipateQuotaFail/:key" />
                
                <Route component={AboutUs} exact path="/aboutUs" />
                <Route component={TermAndCondition} exact path="/termAndCondition" />
                <Route component = {PrivacyPolicy} exact path="/privacyPolicy" />
                <Route component = {AboutEpoints} exact path = "/aboutEpoints" />
              </Switch>
            </div>
            <div>
              {/* <Footer /> */}
            </div>
          </div>
      </Router>
      </div>
      </div>
    );
  }
}


export default App;
