import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {setRegistrationMessage} from '../../actions/registration';
import {fetchProfile} from '../../actions/profile';
import {connect} from 'react-redux';
import "./style.css";
import appConfig from '../../api/apiConfig';
class Menu extends Component {
    constructor(props){
      super(props)
      this.state={
       
      }
        this.setMessage = this.setMessage.bind(this);
    }
    setMessage(){
      this.props.dispatch(setRegistrationMessage());
    }

    

    componentWillReceiveProps(nextProps){
      if(nextProps.userProfile && nextProps.userProfile.length>0){
        let userData = nextProps.userProfile;
        if(userData[0].ImageStatus === true){
          
          sessionStorage.setItem('userImagePath', appConfig.imageUrl + userData[0].ImageURL);

        }else{

        }

      }else if(nextProps.authenticated === undefined || nextProps.authenticated ===false){
        
      }else{
        let access_token=sessionStorage.getItem("token");
        let userId=sessionStorage.getItem("user_id");
        if(userId){
          this.props.dispatch(fetchProfile(userId, access_token));
        }
      }
    }

  render(){
  let menuItem ="";

  let userview = "";

  if(this.props.setUser===undefined || this.props.setUser===false){
    if(sessionStorage.getItem('userImagePath')){
      userview = <div><img src={sessionStorage.getItem('userImagePath')} alt="" class="avatar" /></div>
    }else if(sessionStorage.getItem('user_name')){
      let user =sessionStorage.getItem('user_name').split(" ")[0].slice(0,1).toUpperCase() + sessionStorage.getItem('user_name').split(" ")[1].slice(0,1).toUpperCase();
      userview = <div className ="user">{user}</div>
    }
  }else{
    if(sessionStorage.getItem('userImagePath')){
      userview = <div ><img src={sessionStorage.getItem('userImagePath')} alt="" class="avatar" /></div>
    }else if(sessionStorage.getItem('user_name')){
      let user = sessionStorage.getItem('user_name').split(" ")[0].slice(0,1).toUpperCase() + sessionStorage.getItem('user_name').split(" ")[1].slice(0,1).toUpperCase();
      userview = <div className ="user">{user}</div>
    }
  }

 

  if(this.props.authenticated===undefined || this.props.authenticated===false){
  let userid = sessionStorage.getItem('user_id');
  let access_token = sessionStorage.getItem('token');
  if(userid && access_token){
    menuItem =  <div >
      <img src={require("./LOGO.png")}  className = "logoimage"/>
                  <Link  to="/Profile" > {userview} </Link>
                  <Link  className = "mm" to="/logout"> Logout </Link>
                  <Link  className = "mm"  to="/profileDashboard"> MY Profile</Link>
                  <Link  className = "mm" to="/Study" > Study </Link>
                  <Link  className = "mm" to="/dashboard" > Dashboard </Link>
                  <Link  className = "mm" to="/transactionHistory" > Transaction History </Link>
                </div>
    }else{
      menuItem =  <div>
        {/* <img src={require("./LOGO.png")}  className = "logoimage"/>
                    <Link  to="/login"> Login </Link>
                    <Link  to="/register" onClick={this.setMessage} >New Member Register </Link>
                    <Link  to="/home" >Home</Link> */}
                  </div>
   }
  }else{
      menuItem =  <div >
        <img src={require("./LOGO.png")}  className = "logoimage"/>
                    <Link  className = "mm" to="/Profile" > {userview} </Link>
                    <Link  className = "mm"  to="/logout"> Logout </Link>
                    <Link  className = "mm" to="/profileDashboard"> MY Profile</Link>
                    <Link  className = "mm" to="/Study" > Study </Link>
                    <Link  className = "mm" to="/dashboard" > Dashboard </Link>
                    <Link  className = "mm" to="/transactionHistory" > Transaction History </Link>
                  </div>
        }

       


  return (<div>
    <div className = "menuBar">
      {menuItem}
      </div>
      </div>
   );
  }
}

const  mapStateToProps =(state) =>{
  return {
    authenticated : state.login.authenticated,
    setUser : state.profile.setUser,
    userProfile : state.profile.userProfile
  }
}

export default connect(mapStateToProps)(Menu);
