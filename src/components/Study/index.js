import React, { Component } from 'react';
import Pagination from "react-js-pagination";
//import './style.css';

class StudyList extends Component {
  constructor(props){
    super(props)

    this.state={
      activePage: 1,
      showButton : false,
      totalStudies : 0, 
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(pageNumber) {
    
    this.setState({activePage: pageNumber});
    this.props.getStudiesByPageNo(pageNumber);
  }

  componentDidMount(){
    this.props.getStudiesByPageNo(1);
    this.setState({totalStudies: this.props.totalStudies});
  }

  componentWillReceiveProps(nextProps){
    this.setState({totalStudies: nextProps.totalStudies});
  }


  participate(data){
    let userId=sessionStorage.getItem("user_id");
    //let url ="http://localhost:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;

    //let url ="http://192.169.139.127:4000/CompaneyParticipated/s:"+ data._id + "/u:"+ userId ;
    //let url =  data.study_url +"/s:"+ data._id + "#u:"+ userId ;
    let url="";
    if(data.study_url.match("{uid}")){
      url =  data.study_url.split("{uid}")[0] +"$SID$"+ data._id + "@UID@"+ userId ;
    }else{
      url =  data.study_url +"$SID$"+ data._id + "@UID@"+ userId ;
    }

    window.open(url);
    let participateData ={};
    participateData.studyId = data._id;
    participateData.statusId = 0;
    this.props.saveStudyParticipate(participateData);
  }

  render() {
    let studyList="";
    let studyItem = "";
    let srNo = (this.state.activePage*10 - 10);
    if(this.props.studies && this.props.studies.length){
      let studies = this.props.studies;
      studyList = Object.keys(studies).map((item, index) => {
        studyItem = studies[item];
        let showParticipateButton = "";
         if(studyItem.is_live == true){
          studyItem.isLive = "right";
          }else{
          studyItem.isLive = "wrong";
          }
          if(studyItem.completionStatus === "0"){
            studyItem.completionStatuss = "Started";
            this.state.showButton = true;
          }else if(studyItem.completionStatus === "1"){
            studyItem.completionStatuss = "Completed";
            this.state.showButton = true;
          }else if(studyItem.completionStatus === "2"){
            this.state.showButton = true;
            studyItem.completionStatuss = "Quota Fail";
          }else if(studyItem.completionStatus === "3"){
            this.state.showButton = true;
            studyItem.completionStatuss = "Fail";
          }else {
            studyItem.completionStatus = "";
            this.state.showButton = false;
            showParticipateButton = <button  className ="participatbtn" type="button" onClick={this.participate.bind(this, studyItem)} disabled ={ this.state.showButton}> 
            Participate </button>
          }
          //className = "participatbtn"
          
          srNo = srNo+1;
        return <tr className ="studyTr" key={index}>
          <td className = "studyTd">{srNo}</td>
          <td className = "studyTd">
          <div class="tooltipFirstParty">{studyItem.study_name.slice(0,40)}
          <span class="tooltiptextFirstParty">{studyItem.study_url}</span>
          </div>
          
          </td>
          <td className = "studyTd">{studyItem.cpi}</td>
          <td className = "studyTd">{studyItem.completionStatuss}</td>
          <td className = "studyTd">{studyItem.created_at.split("T")[0]}</td>
          <td className = "studyTd">
            {/* <div><button  className ="tooltipFirstParty" type="button" onClick={this.participate.bind(this, studyItem)} disabled ={ this.state.showButton} > Participate </button>
            <span class="tooltiptextFirstParty">{studyItem.study_url}</span>
            </div> */}
            {showParticipateButton}
          </td>
        </tr>
       
      });
    }

    return (
      
        <div className = "containerS">
        <div className = "studyback">
        <div className = "headingS"><h1> Studies </h1></div>
        <div className = "studytable">
        <table className = "tab">
        <tr className = "trSHeading">
        <th className = "studyTh">Sr. No.</th>
        <th className = "studyTh">Study Name</th>
        <th className = "studyTh">Reward</th>
        <th className = "studyTh">Participation Status</th>
        <th className = "studyTh">Created At</th>
        <th className = "studyTh">Action</th>
        </tr>
        {studyList}
        </table>
        </div>
        <div className="paginationCountryMemberTable">
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={this.state.totalStudies}    //total number of members
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
        </div>
        </div>
        </div>
      
    );
  }
}
export default StudyList;
