import React, { Component } from 'react';
//import './style.css';

class CollectTotalReward extends Component {
  constructor(props){
    super(props)

    this.state={
      errors: {},
      totalRewards: "",
      totalAmount: "",
      couponTotalAmount : "",
      couponTotalPoint : "",
      paymentMethod: "",
      paypalEmail:"",
      amazoneDelivery:"",
      amazonePayAddOne: "",
      amazonePayAddTwo:"",
      amazonePayCity:"",
      amazonePayState: "",
      amazonePayPinCode : "",
      show: false,
    }   
    this.onChange = this.onChange.bind(this); 
    this.paypalTransaction = this.paypalTransaction.bind(this);
    this.giftCouponOnline = this.giftCouponOnline.bind(this);
    this.giftCouponOffline = this.giftCouponOffline.bind(this);
  }
  
  componentWillReceiveProps(nextprops){
    if(nextprops.transactionSuccess){
      
    }
  }

  

  componentWillMount(Data){
      this.setState({
        totalRewards : this.props.StudyCountData.totalRewards,
        totalAmount :  this.props.StudyCountData.totalRewards/50,
      });
  }

  onChange(e){
    this.setState({[e.target.name]: e.target.value});
      if(e.target.name === "paymentMethod"){
        this.setState({amazoneDelivery: ""});

        //make amount and point for gift coupon
        let oldTotalAmount = parseInt(this.state.totalAmount);  
        let newTotalAmount = oldTotalAmount - (oldTotalAmount%10);
        this.setState({ couponTotalAmount : newTotalAmount, couponTotalPoint: newTotalAmount*50 });
      }
    }

    handleValidationPaypalTransaction(){
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      //Email
      if(!this.state.paypalEmail){
         formIsValid = false;
         errors["email"] = "Cannot be empty";
      }else if (!this.state.paypalEmail.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }

     this.setState({errors: errors});
     return formIsValid;
 }

    paypalTransaction(e){    //paypalTransaction
      e.preventDefault();
      if(this.handleValidationPaypalTransaction()){
        if(this.state.totalAmount == "0"){
          alert("Sorry you have 0 balance to redeem");
        }else{
          let paypalTransactionObj = {}
        paypalTransactionObj.user_id = sessionStorage.getItem("user_id");
        paypalTransactionObj.totalPoints = this.state.totalRewards;
        paypalTransactionObj.totalAmount =  this.state.totalAmount;
        paypalTransactionObj.paymentType = "paypal"
        paypalTransactionObj.paypalEmailId = this.state.paypalEmail;    
       this.props.transaction(paypalTransactionObj);
       this.setState({errors: {}});
        }
        
     }
    }

    giftCouponOnline(){     //giftCouponOnline
      let giftCouponObj = {}
      giftCouponObj.user_id = sessionStorage.getItem("user_id");
      giftCouponObj.totalPoints = this.state.couponTotalPoint;
      giftCouponObj.totalAmount =  this.state.couponTotalAmount;
      giftCouponObj.paymentType = "GiftCouponOnline"
      giftCouponObj.deliveryType = "online"
      
      this.props.transaction(giftCouponObj);
    }

    handleValidationGiftCouponOffline(){
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      //amazonePayAddOne
      if(!this.state.amazonePayAddOne){
         formIsValid = false;
         errors["addressOne"] = "Cannot be empty";
      }
      //amazonePayAddTwo
      if(!this.state.amazonePayAddTwo){
        formIsValid = false;
        errors["addressTwo"] = "Cannot be empty";
      }
     //amazonePayState
     if(!this.state.amazonePayState){
      formIsValid = false;
      errors["state"] = "Cannot be empty";
      }
      //amazonePayCity
      if(!this.state.amazonePayCity){
        formIsValid = false;
        errors["city"] = "Cannot be empty";
      }
      //amazonePayPinCode
      if(!this.state.amazonePayPinCode){
        formIsValid = false;
        errors["pinCode"] = "Cannot be empty";
      }else if (!this.state.amazonePayPinCode.match(/^[0-9]{1,6}$/)) {
        formIsValid = false;
        
        errors["pinCode"] = "Pin code should be 6 digits";
      }

          this.setState({errors: errors});
          return formIsValid;
      }

    giftCouponOffline(e){

      e.preventDefault();
      if(this.handleValidationGiftCouponOffline()){
      let giftCouponObj = {}
      giftCouponObj.user_id = sessionStorage.getItem("user_id");
      giftCouponObj.totalPoints = this.state.totalRewards;
      giftCouponObj.totalAmount =  this.state.totalAmount;
      giftCouponObj.paymentType = "GiftCouponOffline"
      giftCouponObj.deliveryType = "offline"
      giftCouponObj.addressLineOne = this.state.amazonePayAddOne;
      giftCouponObj.addressLineTwo =  this.state.amazonePayAddTwo;
      giftCouponObj.state = this.state.amazonePayState;
      giftCouponObj.city =  this.state.amazonePayCity;
      giftCouponObj.pinCode = this.state.amazonePayPinCode;
    
      //this.props.transaction(giftCouponObj);
      }
    }

  render() {

    let showPaymentMethod = "";
    let showAmazoneGiftCardPayment = "";

    if(this.state.paymentMethod === "paypal"){
      showPaymentMethod =   <form onSubmit ={this.paypalTransaction} >
                            <div className = "paypalMethod">
                            <label> Paypal email'id : </label> 
                            <input type="text" className = "paypalEmail" placeholder=" Email"  name="paypalEmail" value={this.state.paypalEmail} onChange={this.onChange}/>
                            <span style={{color: "red", fontSize:"14px"}}>{this.state.errors["email"]}</span><br/><br/>
                            <button type = "submit" className = "collectRewardbtn"> Send Request </button>
                          </div></form>
    }
    if(this.state.paymentMethod === "amazone"){
      showPaymentMethod = <div> 
                            <label className = "amazeLabel">Gift coupons amount :</label><label>{this.state.couponTotalAmount+" USD"}</label><br/>
                            <label className = "amazeLabel">Gift coupons points :</label><label>{this.state.couponTotalPoint}</label><br/><br/><br/>
                            <label className = "amazeLabel">Delivery type :</label>
                            <input type="radio" name="amazoneDelivery" value="Online"  onChange={this.onChange} /> Online 
                            <input type="radio" name="amazoneDelivery" value="Offline"  onChange={this.onChange} /> Offline
                            <br/><br/>
                          </div>
    }

    if(this.state.amazoneDelivery === "Online"){

      showAmazoneGiftCardPayment = <div className = "amazeOffline"> <button className="collectRewardbtn" onClick = {this.giftCouponOnline}>Send Request</button>
      </div>

    }
    if(this.state.amazoneDelivery === "Offline"){
      showAmazoneGiftCardPayment =  <form onSubmit ={this.giftCouponOffline} >
                                      <div className = "amazeOffline"><br/>
                                      <label > Delivery Address  </label><br/><br/>
                                      <label >Address line one :</label><input type="text" className = "" placeholder="Address line one"  name="amazonePayAddOne" value={this.state.amazonePayAddOne} onChange={this.onChange}/>
                                      &nbsp;<span style={{color: "red", fontSize:"14px"}}>{this.state.errors["addressOne"]}</span><br/><br/>
                                      <label> Address line two :</label><input type="text" className = "" placeholder="Address line two"  name="amazonePayAddTwo" value={this.state.amazonePayAddTwo} onChange={this.onChange}/>
                                      &nbsp;<span style={{color: "red", fontSize:"14px"}}>{this.state.errors["addressTwo"]}</span><br/><br/>
                                      <label > City :</label><input type="text" className = "" placeholder="City"  name="amazonePayCity" value={this.state.amazonePayCity} onChange={this.onChange}/>
                                      &nbsp;<span style={{color: "red", fontSize:"14px"}}>{this.state.errors["city"]}</span><br/><br/>
                                      <label> State :</label><input type="text" className = "" placeholder="State"  name="amazonePayState" value={this.state.amazonePayState} onChange={this.onChange}/>
                                      &nbsp;<span style={{color: "red", fontSize:"14px"}}>{this.state.errors["state"]}</span><br/><br/>
                                      <label > PinCode :</label><input type="text" className = "" placeholder="pinCode"  name="amazonePayPinCode" value={this.state.amazonePayPinCode} onChange={this.onChange}/>
                                      &nbsp;<span style={{color: "red", fontSize:"14px"}}>{this.state.errors["pinCode"]}</span><br/><br/>
                                      <button type="submit" >Send Request</button> 
                                    </div></form>
    }

  

    return (
      <div className = "countForm">
        <div className = "collectRewardBack">
         <div className = "headercount"><h1>Point Redeemtion </h1></div>
          <div className = "rewardTr"><br/> <br/>        
          <tr><td>Total points : </td><td>{this.state.totalRewards}</td></tr>
          <tr><td>Total payable amount :</td><td> {this.state.totalAmount+ " USD"}</td></tr>
          <tr><td><h3>Payment method : </h3></td>
            <td>
              <input type="radio" name="paymentMethod" value="paypal"  onChange={this.onChange} /> Paypal &nbsp;&nbsp;
              <input type="radio" name="paymentMethod" value="amazone"  onChange={this.onChange} /> Gift coupons
            </td>
          </tr>
          </div>
            <div><br/>
              {showPaymentMethod}
              {showAmazoneGiftCardPayment}
                
            </div>
          </div>
      </div>
    );  
  }
}
export default CollectTotalReward;
