import React, { Component } from 'react';
//import './style.css';

class TransactionHistoryView extends Component {
  constructor(props){
    super(props)

    this.state={
      
    };
  }

  componentDidMount(){
    document.title = "Panelreward-Transaction"
  }


  getGiftcouponByUser(data){
    this.props.getGiftcouponByUser(data);
  }

  

  render() {
    let studyList="";
    let transactionItem = "";
    let SNo =0;
    if(this.props.UserTranactionData && this.props.UserTranactionData.length){
      let studies = this.props.UserTranactionData;
      studyList = Object.keys(studies).map((item, index) => {
        transactionItem = studies[item];

        let viewAction ="";
         if(transactionItem.paymentType === "GiftCouponOnline" && transactionItem.paymentStatus === "success"){
          viewAction = <button className = "participatbtn" type="button" onClick={this.getGiftcouponByUser.bind(this, transactionItem)}>ViewCoupon</button>
         }else{
          viewAction = <div>{transactionItem.paymentStatus.toUpperCase()}</div>
         }
          
        SNo = SNo+1;
        return <tr className ="studyTr" key={index}>
          <td className = "txnTD">{SNo}</td>
          <td className = "txnTD">{transactionItem.totalAmount+" USD"}</td>
          <td className = "txnTD">{transactionItem.totalPoints}</td>
          <td className = "txnTD">{transactionItem.paymentType}</td>
          <td className = "txnTD">{transactionItem.paymentStatus}</td>
          <td className = "txnTD">{transactionItem.updatedAt.split("T")[0] + " "+transactionItem.updatedAt.split("T")[1].split(".")[0] }</td>
          <td>{viewAction}</td>
          </tr>
      });
    }

    return (
      
        <div className = "containerS">
        <div className = "studyback">
        <div className = "headingS"><h1> Transaction History </h1></div>
            <div className = "studytable">
            <table className = "tab">
            <tr className = "">
            <th style={{textAlign:"center"}}>S.no.</th>
            <th style={{textAlign:"center"}}>Amount</th>
            <th style={{textAlign:"center"}}>Points</th>
            <th style={{textAlign:"center"}}>Payment Type</th>
            <th style={{textAlign:"center"}}>Payment Status</th>
            <th style={{textAlign:"center"}}>Date & Time</th>
            <th style={{textAlign:"center"}}>Action</th>
            </tr>
            {studyList}
            </table>
            </div>
        </div>
        </div>
      
    );
  }
}
export default TransactionHistoryView;
