import React, { Component } from 'react';
//import './style.css';

class GiftcouponView extends Component {
  constructor(props){
    super(props)

    this.state={
      
    };
    this.showTranactions = this.showTranactions.bind(this);
  }

  componentDidMount(){
    document.title = "Panelreward-ViewCoupon"
  }

  showTranactions(){
    this.props.showTranactions();
  }
  

  render() {
    if(this.props.GiftcouponDataSuccess){
      console.log(this.props.GiftcouponData.couponDetails);
    }

    let studyList="";
    let transactionItem = "";
    let SNo =0;
    if(this.props.GiftcouponData && this.props.GiftcouponData.couponDetails.length){
      let studies = this.props.GiftcouponData.couponDetails;
      studyList = Object.keys(studies).map((item, index) => {
        transactionItem = studies[item];

        SNo = SNo+1;
        return <tr className ="studyTr" key={index}>
          <td className = "txnTD">{SNo}</td>
          <td className = "txnTD">{transactionItem[0].Coupon_code}</td>
          <td className = "txnTD">{transactionItem[0].Coupon_amount}</td>
          <td className = "txnTD">{transactionItem[0].Expire_date.split("T")[0] }</td>
          </tr>
      });
    }
    
    return (
      
        <div className = "containerS">
        <div className = "studyback">
        <div className = "headingS"><h1>Assign Coupons </h1></div>
        <br/><button  className = "backbutton" onClick = {this.showTranactions} >Back</button>
            <div className = "studytable">
            <table className = "tab"> 
            <tr className = "">
            <th className = "">S.no.</th>
            <th className = "">Coupon code</th>
            <th className = "">Coupon amount</th>
            <th className = "">Expire date</th>
            </tr>
            {studyList}
            </table>
            </div>
        </div>
        </div>
      
    );
  }
}
export default GiftcouponView;
