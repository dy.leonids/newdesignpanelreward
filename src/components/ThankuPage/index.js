import React, { Component } from 'react';
//import './style.css';
import {Link} from 'react-router-dom';

class ThankuView extends Component {
  constructor(props){
    super(props)
    this.state={
      email: '',
      name: ''
    };

  }

  componentDidMount(){
    
    let userEmail = sessionStorage.getItem('registerUserMail');
    let userName = sessionStorage.getItem('registerUserName');
      

      this.setState({
        email : userEmail,
        name  : userName
      });
    
   
  }


  render(){

    return (
      <div  className = "profilepage">
          <div className = "thankupage">
          <div className = "thankuheader">Validation E-Mail Sent!</div>
          <div className = "thankupara">An e-mail has been sent to<br/> <h4>{this.state.email}</h4></div>
          <div className = "thankpara1">to confirm the ownership of the address by<b> {this.state.name} </b><br/><br/>
            It will arrive in the inbox shortly.<br/> Please follow the instructions in the e-mail.
          </div>
          <div className="sign">
          <p ><span> <Link to="/login">Login From Here </Link></span></p>
          </div>
       </div>
      
      </div>
      
    );
  }
}
export default ThankuView;
