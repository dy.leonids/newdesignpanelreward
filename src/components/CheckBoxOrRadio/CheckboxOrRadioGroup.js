import React from 'react';
import PropTypes from 'prop-types';

const CheckboxOrRadioGroup = (props) => (


	<div>
		{/* <label className="form-label">{props.title}</label> */}
		{/* <div className="checkbox-group"> */}

			{props.options.options.map(option => {

				//console.log("option",option);
				let selected =false;
				if(props.selectedOptions !== undefined && props.selectedOptions.length>0){
					selected =( props.selectedOptions.indexOf(option._id) > -1);
				}
				return (
					<div key={"check"+option._id}>
					{/* <label className="form-label capitalize"> */}
						<input
							className="form-checkbox"
							name={"test"}
							onChange={props.controlFunc}
							value={option.option_id}
							checked={selected}
							type={props.type} /> 
							{option.options}
					{/* </label> */}
					</div>
				);
			})}
		{/* </div> */}
	</div>
);

CheckboxOrRadioGroup.propTypes = {
	title: PropTypes.string.isRequired,
	type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
	setName: PropTypes.string.isRequired,
	options: PropTypes.array.isRequired,
	selectedOptions: PropTypes.array,
	controlFunc: PropTypes.func.isRequired
};

export default CheckboxOrRadioGroup;
