import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
//import Modal from 'react-modal';
import Modal from "react-responsive-modal";
//import './style.css';

import imageuploads from '../imageupload/imageuploads';


var datacountryList = require('../../utils/country.json');

class Register extends Component {
  constructor(props){
    super(props)

    this.state={
      email :"",
      contact:"",
      myimg:"",
      password  :"",
      firstName:"",
      lastName:"",
      dob:"",
      gender:"",
      addressLineOne: "",
      addressLineTwo: "",
      state : "",
      country : "",
      pinCode : "",
      marriageStatus: "",
      confirmPassword: "",
      isActive: false,
      check:true,
      open: false,
      check1:true,
      open1: false,
      countryData: [],
      countryCode:'',
    }
    this.onChange = this.onChange.bind(this);
    this.saveData = this.saveData.bind(this);
    this.loginPush = this.loginPush.bind(this);
    this.checkfunction = this.checkfunction.bind(this);
    this.checkfunction1 = this.checkfunction1.bind(this);
  } 

  onChange(e){
    if(e.target.name == "contact"){
      if( (e.target.value).includes(this.state.countryCode) ){
        this.setState({[e.target.name]: e.target.value});
      }else if(e.target.value.length < this.state.countryCode.length && (e.target.value).includes('+')  ){
        this.setState({[e.target.name]: ''});
      }else{
        this.setState({[e.target.name]: this.state.countryCode + e.target.value});
      }
    }else{
      this.setState({[e.target.name]: e.target.value});
    }
    
    if(e.target.name === 'country' && e.target.value !== '' && e.target.value !== null){
      let countryCodeNo = this.state.countryData.find(item => item.name == e.target.value).code;
      this.setState({countryCode: countryCodeNo , contact: ''});
    }else if(e.target.name === 'country' && e.target.value !== ''){
      this.setState({countryCode: ''});
    }

    if(this.state.contact == "contact"){

    }
  }

  // componentWillMount(){
  //   Modal.setAppElement('body');
  // }

  checkfunction(){

    this.setState({
      check:!this.state.check
    })
  }

  checkfunction1(){
    this.setState({
      check1:!this.state.check1
    })
  }

  togglemodal1 = () =>{
    this.setState({ open1: true });
  }

  togglemodal = () =>{
    this.setState({ open: true });
  }


  componentDidMount(){
      this.setState({ countryData : 
        [
          {"name": "Afghanistan", "iso2": "AF", "code": "+93"},
          {"name": "Albania", "iso2": "AL", "code": "+355"},
          {"name": "Algeria", "iso2": "DZ", "code": "+213"},
          {"name": "American Samoa", "iso2": "AS", "code": "+1-684"},
          {"name": "Andorra", "iso2": "AD", "code": "+376"},
          {"name": "Angola", "iso2": "AO", "code": "+244"},
          {"name": "Anguilla", "iso2": "AI", "code": "+1-264"} ,
          {"name": "Antarctica","iso2": "AQ","code": "+672"},
          {"name": "Antigua And Barbuda", "iso2": "AG", "code": "+1-268" },
          {"name": "Argentina", "iso2": "AR", "code": "+54" },
          {"name": "Armenia", "iso2": "AM", "code": "+374" },
          {"name": "Aruba", "iso2": "AW","code": "+297"},
          {"name": "Ascension Island", "iso2": "AC","code": "+247"},
          {"name": "Australia", "iso2": "AU", "code": "+61"},
          {"name": "Austria", "iso2": "AT", "code": "+43"},
          {"name": "Azerbaijan", "iso2": "AZ", "code": "+994"},
          {"name": "Bahamas", "iso2": "BS", "code": "+1-242"},
          {"name": "Bahrain", "iso2": "BH", "code": "+973"},
          {"name": "Bangladesh", "iso2": "BD", "code": "+880"},
          {"name": "Barbados", "iso2": "BB", "code": "+1-246"},
          {"name": "Belarus", "iso2": "BY", "code": "+375" },
          {"name": "Belgium", "iso2": "BE", "code": "+32"},
          {"name": "Belize", "iso2": "BZ", "code": "+501"},
          {"name": "Benin", "iso2": "BJ", "code": "+229"},
          {"name": "Bermuda", "iso2": "BM", "code": "+1-441"},
          {"name": "Bhutan", "iso2": "BT", "code": "+975"},
          {"name": "Bolivia, Plurinational State Of", "iso2": "BO", "code": "+591"},
          {"name": "Bonaire, Saint Eustatius And Saba", "iso2": "BQ", "code": "+599"},
          {"name": "Bosnia & Herzegovina", "iso2": "BA", "code": "+387" },
          {"name": "Botswana", "iso2": "BW", "code": "+267"},
          {"name": "Brazil", "iso2": "BR", "code": "+55"},
          {"name": "British Indian Ocean Territory", "iso2": "IO", "code": "+246" },
          {"name": "Brunei Darussalam", "iso2": "BN", "code": "+673" },
          {"name": "Bulgaria", "iso2": "BG", "code": "+359"},
          {"name": "Burkina Faso", "iso2": "BF", "code": "+226" },
          {"name": "Burundi", "iso2": "BI", "code": "+257" },
          {"name": "Cambodia", "iso2": "KH", "code": "+855" },
          {"name": "Cameroon", "iso2": "CM", "code": "+237" },
          {"name": "Canada", "iso2": "CA", "code": "+1" },
          {"name": "Cape Verde", "iso2": "CV", "code": "+238" },
          {"name": "Cayman Islands", "iso2": "KY", "code": "+1-345" },
          {"name": "Central African Republic", "iso2": "CF", "code": "+236" },
          {"name": "Chad","iso2": "TD","code": "+235"},
          {"name": "Chile","iso2": "CL","code": "+56"},
          {"name": "China","iso2": "CN","code": "+86"},
          {"name": "Christmas Island","iso2": "CX","code": "+61"},
          {"name": "Cocos (Keeling) Islands","iso2": "CC","code": "+61"},
          {"name": "Colombia","iso2": "CO","code": "+57"},
          {"name": "Comoros","iso2": "KM","code": "+269"},
          {"name": "Cook Islands","iso2": "CK","code": "+682"},
          {"name": "Costa Rica","iso2": "CR","code": "+506"},
          {"name": "Cote d'Ivoire","iso2": "CI","code": "+225"},
          {"name": "Croatia","iso2": "HR","code": "+385"},
          {"name": "Cuba","iso2": "CU","code": "+53"},
          {"name": "Curacao","iso2": "CW","code": "+599"},
          {"name": "Cyprus","iso2": "CY","code": "+357"},
          {"name": "Czech Republic","iso2": "CZ","code": "+420"},
          {"name": "Democratic Republic Of Congo","iso2": "CD","code": "+243"},
          {"name": "Denmark","iso2": "DK","code": "+45"},
          {"name": "Djibouti","iso2": "DJ","code": "+253"},
          {"name": "Dominica","iso2": "DM","code": "+1-767"},
          {"name": "Dominican Republic","iso2": "DO","code": "+1-809"},
          {"name": "East Timor","iso2": "TL","code": "+670"},
          {"name": "Ecuador","iso2": "EC","code": "+593"},
          {"name": "Egypt","iso2": "EG","code": "+20"},
          {"name": "El Salvador","iso2": "SV","code": "+503"},
          {"name": "Equatorial Guinea","iso2": "GQ","code": "+240"},
          {"name": "Eritrea","iso2": "ER","code": "+291"},
          {"name": "Estonia","iso2": "EE","code": "+372"},
          {"name": "Ethiopia","iso2": "ET","code": "+251"},
          {"name": "European Union","iso2": "EU","code": "+388"},
          {"name": "Falkland Islands","iso2": "FK","code": "+500"},
          {"name": "Faroe Islands","iso2": "FO","code": "+298"},
          {"name": "Fiji","iso2": "FJ","code": "+679"},
          {"name": "Finland","iso2": "FI","code": "+358"},
          {"name": "France","iso2": "FR","code": "+33"},
          {"name": "France, Metropolitan","iso2": "FX","code": "+241"},
          {"name": "French Guiana","iso2": "GF","code": "+44"},
          {"name": "French Polynesia","iso2": "PF","code": "+689"},
          {"name": "Gabon","iso2": "GA","code": "+44"},
          {"name": "Gambia","iso2": "GM","code": "+220"},
          {"name": "Georgia","iso2": "GE","code": "+594"},
          {"name": "Germany","iso2": "DE","code": "+49"},
          {"name": "Ghana","iso2": "GH","code": "+233"},
          {"name": "Gibraltar","iso2": "GI","code": "+350"},
          {"name": "Greece","iso2": "GR","code": "+30"},
          {"name": "Greenland","iso2": "GL","code": "+299"},
          {"name": "Grenada","iso2": "GD","code": "+995"},
          {"name": "Guadeloupe","iso2": "GP","code": "+590"},
          {"name": "Guam","iso2": "GU","code": "+1 671"},
          {"name": "Guatemala","iso2": "GT","code": "+502"},
          {"name": "Guinea","iso2": "GN","code": "+224"},
          {"name": "Guinea-bissau","iso2": "GW","code": "+245"},
          {"name": "Guyana","iso2": "GY","code": "+592"},
          {"name": "Haiti","iso2": "HT","code": "+509"},
          {"name": "Honduras","iso2": "HN","code": "+504"},
          {"name": "Hong Kong","iso2": "HK","code": "+852"},
          {"name": "Hungary","iso2": "HU","code": "+36"},
          {"name": "Iceland","iso2": "IS","code": "+354"},
          {"name": "India","iso2": "IN","code": "+91"},
          {"name": "Indonesia","iso2": "ID","code": "+62"},
          {"name": "Iran, Islamic Republic Of","iso2": "IR","code": "+98"},
          {"name": "Iraq","iso2": "IQ","code": "+964"},
          {"name": "Ireland","iso2": "IE","code": "+353"},
          {"name": "Isle Of Man","iso2": "IM","code": "+44"},
          {"name": "Israel","iso2": "IL","code": "+972"},
          {"name": "Italy","iso2": "IT","code": "+39"},
          {"name": "Jamaica","iso2": "JM","code": "+1-876"},
          {"name": "Japan","iso2": "JP","code": "+81"},
          {"name": "Jersey","iso2": "JE","code": "+44"},
          {"name": "Jordan","iso2": "JO","code": "+962"},
          {"name": "Kazakhstan","iso2": "KZ","code": "+7"},
          {"name": "Kenya","iso2": "KE","code": "+254"},
          {"name": "Kiribati","iso2": "KI","code": "+686"},
          {"name": "Korea, Democratic People's Republic Of","iso2": "KP","code": "+850"},
          {"name": "Korea, Republic Of","iso2": "KR","code": "+82"},
          {"name": "Kuwait","iso2": "KW","code": "+965"},
          {"name": "Kyrgyzstan","iso2": "KG","code": "+996"},
          {"name": "Lao People's Democratic Republic","iso2": "LA","code": "+856"},
          {"name": "Latvia","iso2": "LV","code": "+371"},
          {"name": "Lebanon","iso2": "LB","code": "+961"},
          {"name": "Lesotho","iso2": "LS","code": "+266"},
          {"name": "Liberia","iso2": "LR","code": "+231"},
          {"name": "Libya","iso2": "LY","code": "+218"},
          {"name": "Liechtenstein","iso2": "LI","code": "+423"},
          {"name": "Lithuania","iso2": "LT","code": "+370"},
          {"name": "Luxembourg","iso2": "LU","code": "+352"},
          {"name": "Macao","iso2": "MO","code": "+853"},
          {"name": "Macedonia, The Former Yugoslav Republic Of","iso2": "MK","code": "+389"},
          {"name": "Madagascar","iso2": "MG","code": "+261"},
          {"name": "Malawi","iso2": "MW","code": "+265"},
          {"name": "Malaysia","iso2": "MY","code": "+60"},
          {"name": "Maldives","iso2": "MV","code": "+960"},
          {"name": "Mali","iso2": "ML","code": "+223"},
          {"name": "Malta","iso2": "MT","code": "+356"},
          {"name": "Marshall Islands","iso2": "MH","code": "+692"},
          {"name": "Martinique","iso2": "MQ","code": "+596"},
          {"name": "Mauritania","iso2": "MR","code": "+222"},
          {"name": "Mauritius","iso2": "MU","code": "+230"},
          {"name": "Mayotte","iso2": "YT","code": "+262"},
          {"name": "Mexico","iso2": "MX","code": "+52"},
          {"name": "Micronesia, Federated States Of","iso2": "FM","code": "+691"},
          {"name": "Moldova","iso2": "MD","code": "+373"},
          {"name": "Monaco","iso2": "MC","code": "+377"},
          {"name": "Mongolia","iso2": "MN","code": "+976"},
          {"name": "Montenegro","iso2": "ME","code": "+382"},
          {"name": "Montserrat","iso2": "MS","code": "+1-664"},
          {"name": "Morocco","iso2": "MA","code": "+212"},
          {"name": "Mozambique","iso2": "MZ","code": "+258"},
          {"name": "Myanmar","iso2": "MM","code": "+95"},
          {"name": "Namibia","iso2": "NA","code": "+264"},
          {"name": "Nauru","iso2": "NR","code": "+674"},
          {"name": "Nepal","iso2": "NP","code": "+977"},
          {"name": "Netherlands","iso2": "NL","code": "+31"},
          {"name": "New Caledonia","iso2": "NC","code": "+687"},
          {"name": "New Zealand","iso2": "NZ","code": "+64"},
          {"name": "Nicaragua","iso2": "NI","code": "+505"},
          {"name": "Niger","iso2": "NE","code": "+227"},
          {"name": "Nigeria","iso2": "NG","code": "+234"},
          {"name": "Niue","iso2": "NU","code": "+683"},
          {"name": "Norfolk Island","iso2": "NF","code": "+672"},
          {"name": "Northern Mariana Islands","iso2": "MP","code": "+1-670"},
          {"name": "Norway","iso2": "NO","code": "+47"},
          {"name": "Oman","iso2": "OM","code": "+968"},
          {"name": "Pakistan","iso2": "PK","code": "+92"},
          {"name": "Palau","iso2": "PW","code": "+680"},
          {"name": "Palestinian Territory, Occupied","iso2": "PS","code": "+970"},
          {"name": "Panama","iso2": "PA","code": "+507"},
          {"name": "Papua New Guinea","iso2": "PG","code": "+675"},
          {"name": "Paraguay","iso2": "PY","code": "+595"},
          {"name": "Peru","iso2": "PE","code": "+51"},
          {"name": "Philippines","iso2": "PH","code": "+63"},    
          {"name": "Poland","iso2": "PL","code": "+48"},
          {"name": "Portugal","iso2": "PT","code": "+351"},
          {"name": "Puerto Rico","iso2": "PR","code": "+1-787"},
          {"name": "Qatar","iso2": "QA","code": "+974"},
          {"name": "Republic Of Congo","iso2": "CG","code": "+242"},
          {"name": "Reunion","iso2": "RE","code": "+262"},
          {"name": "Romania","iso2": "RO","code": "+40"},
          {"name": "Russian Federation","iso2": "RU","code": "+7"},
          {"name": "Rwanda","iso2": "RW","code": "+250"},
          {"name": "Saint Barthélemy","iso2": "BL","code": "+590"},
          {"name": "Saint Helena, Ascension And Tristan Da Cunha","iso2": "SH","code": "+290"},
          {"name": "Saint Kitts And Nevis","iso2": "KN","code": "+1-869"},
          {"name": "Saint Lucia","iso2": "LC","code": "+1-758"},
          {"name": "Saint Martin","iso2": "MF","code": "+590"},
          {"name": "Saint Pierre And Miquelon","iso2": "PM","code": "+508"},
          {"name": "Saint Vincent And The Grenadines","iso2": "VC","code": "+1-784"},
          {"name": "Samoa","iso2": "WS","code": "+685"},
          {"name": "San Marino","iso2": "SM","code": "+378"},
          {"name": "Sao Tome And Principe","iso2": "ST","code": "+239"},
          {"name": "Saudi Arabia","iso2": "SA","code": "+966"},
          {"name": "Senegal","iso2": "SN","code": "+221"},
          {"name": "Serbia","iso2": "RS","code": "+381"},
          {"name": "Seychelles","iso2": "SC","code": "+248"},
          {"name": "Sierra Leone","iso2": "SL","code": "+232"},
          {"name": "Singapore","iso2": "SG","code": "+65"},
          {"name": "Sint Maarten","iso2": "SX","code": "+1-721"},
          {"name": "Slovakia","iso2": "SK","code": "+421"},
          {"name": "Slovenia","iso2": "SI","code": "+386"},
          {"name": "Solomon Islands","iso2": "SB","code": "+677"},
          {"name": "Somalia","iso2": "SO","code": "+252"},
          {"name": "South Africa","iso2": "ZA","code": "+27"},
          {"name": "South Georgia And The South Sandwich Islands","iso2": "GS","code": "+500"},
          {"name": "Spain","iso2": "ES","code": "+34"},
          {"name": "Sri Lanka","iso2": "LK","code": "+94"},
          {"name": "Sudan","iso2": "SD","code": "+249"},
          {"name": "Suriname","iso2": "SR","code": "+597"},
          {"name": "Svalbard And Jan Mayen","iso2": "SJ","code": "+47"},
          {"name": "Swaziland","iso2": "SZ","code": "+268"},
          {"name": "Sweden","iso2": "SE","code": "+46"},
          {"name": "Switzerland","iso2": "CH","code": "+41"},
          {"name": "Syrian Arab Republic","iso2": "SY","code": "+963"},
          {"name": "Taiwan, Province Of China","iso2": "TW","code": "+886"},
          {"name": "Tajikistan","iso2": "TJ","code": "+992"},
          {"name": "Tanzania, United Republic Of","iso2": "TZ","code": "+255"},
          {"name": "Thailand","iso2": "TH","code": "+66"},
          {"name": "Togo","iso2": "TG","code": "+228"},
          {"name": "Tokelau","iso2": "TK","code": "+690"},
          {"name": "Tonga","iso2": "TO","code": "+676"},
          {"name": "Trinidad And Tobago","iso2": "TT","code": "+1-868"},
          {"name": "Tristan de Cunha","iso2": "TA","code": "+290"},
          {"name": "Tunisia","iso2": "TN","code": "+216"},
          {"name": "Turkey","iso2": "TR","code": "+90"},
          {"name": "Turkmenistan","iso2": "TM","code": "+993"},
          {"name": "Turks And Caicos Islands","iso2": "TC","code": "+1-649"},
          {"name": "Tuvalu","iso2": "TV","code": "+688"},
          {"name": "USSR","iso2": "SU","code": "+7"},
          {"name": "Uganda","iso2": "UG","code": "+256"},
          {"name": "Ukraine","iso2": "UA","code": "+380"},
          {"name": "United Arab Emirates","iso2": "AE","code": "+971"},
          {"name": "United Kingdom","iso2": "GB","code": "+1-473"},
          {"name": "United Kingdom","iso2": "UK","code": "+44"},
          {"name": "United States","iso2": "US","code": "+1"},
          {"name": "United States Minor Outlying Islands","iso2": "UM","code": "+1"},
          {"name": "Uruguay","iso2": "UY","code": "+598"},
          {"name": "Uzbekistan","iso2": "UZ","code": "+998"},
          {"name": "Vanuatu","iso2": "VU","code": "+678"},
          {"name": "Vatican City State","iso2": "VA","code": "+379"},
          {"name": "Venezuela, Bolivarian Republic Of","iso2": "VE","code": "+58"},
          {"name": "Viet Nam","iso2": "VN","code": "+84"},
          {"name": "Virgin Islands (British)","iso2": "VG","code": "+1-284"},
          {"name": "Virgin Islands (US)","iso2": "VI","code": "+1-340"},
          {"name": "Wallis And Futuna","iso2": "WF","code": "+681"},
          {"name": "Western Sahara","iso2": "EH","code": "+212"},
          {"name": "Yemen","iso2": "YE","code": "+967"},
          {"name": "Zambia","iso2": "ZM","code": "+260"},
          {"name": "Zimbabwe","iso2": "ZW","code": "+263"}
        ]
      });
  }

  handleValidation() {
		let valid = true;   

		if (this.state.firstName === "") {
		  alert("please enter firstName");
			valid = false;
		}else if (this.state.lastName === "") {
			alert("please enter lastName");
			valid = false;
		}else if (this.state.email === "") {
		   alert("please enter email");
			 valid = false;
    }else if (this.state.contact === "") {
      alert("please enter contact number");
      valid = false;}
    
    else if (isNaN(this.state.contact)) {
      alert("Please enter only number in contact field");
      valid = false;}
    
    else if (this.state.dob === "") {
			alert("please enter dob");
			valid = false;
		}else if (this.state.gender === "") {
			alert("please select gender");
			valid = false;
		}else if (this.state.addressLineOne === "") {
			alert("please enter address LineOne");
			valid = false;
		}else if (this.state.addressLineTwo === "") {
			alert("please enter address LineTwo");
			valid = false;
		}else	if (this.state.country === "") {
			alert("please select country");
			valid = false;
		}else if (this.state.state === "") {
     alert("please enter email");
     valid = false;
   }else if (this.state.pinCode === "") {
			alert("please enter pinCode");
			valid = false;
    }else if (isNaN(this.state.pinCode)) {
      alert("Please enter only number in PinCod field");
      valid = false;}
    else	if (this.state.marriageStatus === "") {
			alert("please select marriageStatus");
			valid = false;
		}else if (this.state.password === "") {
			alert("please enter password");
			valid = false;
		}else if (this.state.confirmPassword === "") {
			alert("please enter confirm password");
			valid = false;
		}else if (this.state.confirmPassword !== this.state.password) {
			alert("Password and confirmPassword didn't match");
			valid = false;
		}
    else if(this.state.check){
      alert("Please check term and conditions !");
      valid = false;
    }
		return valid;
	}

  saveData(){
    let registerData = {}
      if (this.handleValidation()) {
      registerData.registerType = "email";
      registerData.email =  this.state.email;
      registerData.password = this.state.password;
      registerData.firstName =  this.state.firstName;
      registerData.lastName =  this.state.lastName;
      registerData.dob =  this.state.dob;
      registerData.gender =  this.state.gender;
      registerData.addressLineOne =  this.state.addressLineOne;
      registerData.addressLineTwo =  this.state.addressLineTwo;
      registerData.state =  this.state.state;
      registerData.country =  this.state.country;
      registerData.pinCode =  this.state.pinCode;
      registerData.marriageStatus =  this.state.marriageStatus;
      registerData.contact =  this.state.contact;
      this.props.saveData(registerData);
    }
  }

  loginPush(){
    this.props.loginPush();
  }


  onCloseModal = () => {
    this.setState({ open: false });
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };

  render() {

     const { open, open1 } = this.state;  

      let resgisMsg = "";
      if(this.props.registered=== true){
      //  resgisMsg = "user is registerd.";
        //this.loginPush();
      }else{
          if(this.props.failMessage){
            resgisMsg = this.props.failMessage;
          }
      }


      let countries = this.state.countryData;
      let countriesItem = countries.map((country) =>
              <option key={country.name}>{country.name}</option>
          );



    return (
      <div className = "regPage"> 
      <div className = "form">
      <div className = "GNNheading"><h1>Panelreward Registration Form</h1></div>
          
          
         <div className = "createAccount" > <p > CREATE AN ACCOUNT</p></div>
           
           <div className = "registerform">
           <label>First name</label>
          <input type="text" className = "formInputtextName" placeholder=" First name" name="firstName" value={this.state.firstName} onChange={this.onChange} /><br/>

          <label>Last name</label>
          <input type="text" className = "formInputtextName" placeholder=" Last name" name="lastName" value={this.state.lastName} onChange={this.onChange} /><br/>

          <label>Email </label>
          <input type="text" className = "formInputtextemail" placeholder=" Email"  name="email" value={this.state.email} onChange={this.onChange}/><br/>
          
          <label>D.O.B.</label>
          <input type="date" className = "formInputtextdob inputdob" name="dob"  onChange={this.onChange}/><br/>

          <label>Gender</label>
          <input type="radio" name="gender" value="Male"  onChange={this.onChange} /> Male
          <input type="radio" name="gender" value="Female" className="gen" onChange={this.onChange} /> Female
          <input type="radio" name="gender" value="Transgender" className="gen" onChange={this.onChange} /> Transgender
          <input type="radio" name="gender" value="Shemale" className="gen" onChange={this.onChange} /> Shemale        
          <input type="radio" name="gender" value="Others" className="gen" onChange={this.onChange} /> Others 
          <br/><br/>

          <label> Address  </label><br/>
          <label className = "addLabel"> Line one: </label><input type="text" className = "formAddress" name ="addressLineOne" value={this.state.addressLineOne} onChange={this.onChange} /><br/>

          <label> Line two: </label><input type="text" className = "formAddress" name ="addressLineTwo" value={this.state.addressLineTwo} onChange={this.onChange} /><br/>

          <label> Country</label>
          {/* <input type="text" className = "formAddCountry" name ="country" value={this.state.country} onChange={this.onChange} /><br/> */}
          <select name = "country" className = "formAddCountry"   value={this.state.country} onChange={this.onChange} >
          <option Value="" >-- Please Select --</option>
           {countriesItem}
          </select>
          <br/>

          <label>Contact No. </label>
          {/* <label >{this.state.countryCode}</label> */}
          <input type="text" className = "formInputtextemail" placeholder = "Contact number"  name="contact" value={this.state.contact} onChange={this.onChange}/><br/>
          <label> State</label><input type="text" className = "formAddState" name ="state" value={this.state.state} onChange={this.onChange} /><br/>

          <label> Pincode</label><input type="text" className = "formAddPin" name ="pinCode" value={this.state.pinCode} onChange={this.onChange} /><br/>

          <label>Marriage status</label>
          {/* <input type="radio" name="marriageStatus" value="married"  className="" onChange={this.onChange} /> Married
          <input type="radio" name="marriageStatus" value="unmarried"  className="marri" onChange={this.onChange} /> UnMarried 
          <input type="radio" name="marriageStatus" value="lesbian"  className="marri" onChange={this.onChange} /> Lesbian
          <input type="radio" name="marriageStatus" value="gay"  className="marri" onChange={this.onChange} /> Gay
          <input type="radio" name="marriageStatus" value="divorced"  className="marri" onChange={this.onChange} /> Divorced<br/><br/>
          <input type="radio" name="marriageStatus" value="registerPartner" style={{marginLeft :'18.7%'}} onChange={this.onChange} /> Register Partner
          <input type="radio" name="marriageStatus" value="others "  className="marri" onChange={this.onChange} /> Others  */}

          <select name = "marriageStatus" className = "formAddCountry"   value={this.state.marriageStatus} onChange={this.onChange} >
            <option Value="" >-- Please Select --</option>
            <option Value="married" >Married</option>
            <option Value="unmarried" >UnMarried</option>
            <option Value="lesbian" >Lesbian</option>
            <option Value="gay" >Gay</option>
            <option Value="divorced" >Divorced</option>
            <option Value="registerPartner" >Register Partner</option>
            <option Value="others" >Others</option>
          </select>            

          <br/><br/>
          
          {/*
          <label>Profile image</label>
          <input type="file" className = "formInputtextPass" name="myImage" name="myimg" value={this.state.myimgs}   onChange= {this.onChange} />*/}
          <label>Password</label>
          <input type="password" className = "formInputtextPass" placeholder= " Password" name="password" value={this.state.password} onChange={this.onChange} />

          <label>Confirm Password</label>
          <input type="password" className = "formInputtextPass" placeholder= " Confirm Password" name="confirmPassword" value={this.state.confirmPassword} onChange={this.onChange} />

          <div>
          <div onClick = {this.checkfunction} className = "checkDiv"><input id = "check" type="checkbox" />&nbsp;&nbsp;<button onClick = {this.togglemodal}>Please Check the Terms & Conditions.</button><br/></div>
          <br/>
          <div onClick = {this.checkfunction1} className = "checkDiv"><input id = "check1" type="checkbox" />&nbsp;&nbsp;<button onClick = {this.togglemodal1}>Please Check the Payment Terms & Conditions.</button><br/></div>  
          <label></label>
          <div className = "registerbutton"><button  className="registerbtn" onClick={this.saveData} >Register</button></div><br/>

          <div className="loading">{
               this.props.requestStarted?
               <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
               :
               false
             }</div><br/>
             <div className = "resgisMsg">{resgisMsg}</div>
          </div>
           </div>
          {/* <div className="sign">
          <p >Already have an account? <Link to="/login"> Sign in </Link>.</p>
          </div> */}
          </div>
          <Modal open={open1} onClose={this.onCloseModal1}  >
         
          <h4>About Points :</h4>
          <p style={{textAlign:"left"}}>Once registered to PANEL REWARD Surveys, you will be sent surveys for you to respond to. 
          You will accumulate EPoints depending on the contents, number of questions and length of each survey. 
          Accumulated EPoints are redeemable for rewards such as online shopping gift coupons.</p>
          <p style={{textAlign:"left"}}>50 EPoint =  1 USD.</p>
          <h4>Redeem EPoints :</h4>
          <p style={{textAlign:"left"}}>Currently gift coupons of various amounts are available for you to redeem. The minimum required for redemption is more then 500 EPoints.</p>
          <h5><b>Online Gift Coupon (Starting from Rs. 10)</b></h5><br/>
          <h4 style={{textAlign:"left"}}>About Cash Conversion And Transfer to Paypal Account :</h4>
          <p style={{textAlign:"left"}}>You can transfer your accumulated EPoints to your Paypal account. Minimum required is more then 500 EPoints.</p>
          <p style={{textAlign:"left" , color:"red"}}>*Your account must match your registered E-mail address</p>
         
          </Modal>

          <Modal open={open} onClose={this.onCloseModal} center className="modal">
          <h4>Term and Conditions:</h4>
          <p style={{textAlign:"left"}}>These Website Standard Terms and Conditions (these “Terms” or these “Website Standard Terms and Conditions”) contained herein on this webpage, shall govern your use of this website, including all pages within this website (collectively referred to herein below as this “Website”). These Terms apply in full force and effect to your use of this Website and by using this Website, you expressly accept all terms and conditions contained herein in full. You must not use this Website, if you have any objection to any of these Website Standard Terms and Conditions.</p>
          <p style={{textAlign:"left"}}>This Website is not for use by any minors (defined as those who are not at least 18 years of age), and you must not use this Website if you a minor.</p>
          <h4>Intellectual Property Rights</h4>
          <p style={{textAlign:"left"}}>Other than content you own, which you may have opted to include on this Website, under these Terms, PANEL REWARD and/or its licensors own all rights to the intellectual property and material contained in this Website, and all such rights are reserved.
          You are granted a limited license only, subject to the restrictions provided in these Terms, for purposes of viewing the material contained on this Website.
          PandaTip: If the website is only for viewing, then you might be able to stop where we left off above. That said, you will likely need to provide more in the way of descriptive language around what users may use the website to do/for.
          </p>
          <h4>Restrictions</h4>
          <p style={{textAlign:"left"}}>You are expressly and emphatically restricted from all of the following:</p>
          <ul>
            <li>publishing any Website material in any media</li>
            <li>selling, sublicensing and/or otherwise commercializing any Website material</li>
            <li>publicly performing and/or showing any Website material</li>
            <li>using this Website in any way that is, or may be, damaging to this Website</li>
            <li>using this Website in any way that impacts user access to this Website</li>
            <li>using this Website in any way that impacts user access to this Website</li>
            <li>using this Website contrary to applicable laws and regulations, or in a way that causes, or may cause, harm to the Website, or to any person or business entity</li>
            <li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website, or while using this Website</li>
            <li>using this Website to engage in any advertising or marketing</li>
            <li>Certain areas of this Website are restricted from access by you and PANEL REWARD may further restrict access by you to any areas of this Website, at any time, in its sole and absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality of such information.</li>
          </ul>
          <h4>Your Content</h4>
          <p style={{textAlign:"left"}}>In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video, text, images or other material you choose to display on this Website. With respect to Your Content, by displaying it, worldwide, irrevocable, royalty-free, sublicensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.
            Your Content must be your own and must not be infringing on any third party’s rights. PANEL REWARD reserves the right to remove any of Your Content from this Website at any time, and for any reason, without notice.
          </p>
          <h4>No warranties</h4>
          <p style={{textAlign:"left"}}>This Website is provided “as is,” with all faults, and PANEL REWARD makes no express or implied representations or warranties, of any kind related to this Website or the materials contained on this Website. Additionally, nothing contained on this Website shall be construed as providing consult or advice to you.</p>
          <h4>Limitation of liability</h4>
          <p style={{textAlign:"left"}}>In no event shall PANEL REWARD, nor any of its officers, directors and employees, be liable to you for anything arising out of or in any way connected with your use of this Website, whether such liability is under contract, tort or otherwise, and PANEL REWARD, including its officers, directors and employees shall not be liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>
          <h4>Indemnification</h4>
          <p style={{textAlign:"left"}}>You hereby indemnify to the fullest extent PANEL REWARD from and against any and all liabilities, costs, demands, causes of action, damages and expenses (including reasonable attorney’s fees) arising out of or in any way related to your breach of any of the provisions of these Terms.</p>
          <h4>Severability</h4>
          <p style={{textAlign:"left"}}>If any provision of these Terms is found to be unenforceable or invalid under any applicable law, such unenforceability or invalidity shall not render these Terms unenforceable or invalid as a whole, and such provisions shall be deleted without affecting the remaining provisions herein.</p>
          <h4>Variation of Terms</h4>
          <p style={{textAlign:"left"}}>PANEL REWARD is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review such Terms on a regular basis to ensure you understand all terms and conditions governing use of this Website.</p>
          <h4>Assignment</h4>
          <p style={{textAlign:"left"}}>PANEL REWARD shall be permitted to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification or consent required. However, .you shall not be permitted to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>
          <h4>Entire Agreement</h4>
          <p style={{textAlign:"left"}}>These Terms, including any legal notices and disclaimers contained on this Website, constitute the entire agreement between PANEL REWARD and you in relation to your use of this Website, and supersede all prior agreements and understandings with respect to the same.</p>
          <h4>Governing Law & Jurisdiction </h4>
          <p style={{textAlign:"left"}}>These Terms will be governed by and construed in accordance with the laws of the State of UAE and you submit to the non-exclusive jurisdiction of the state and federal courts located in UAE for the resolution of any disputes.</p>
        </Modal>
      </div>
    );  
  }
}
export default Register;
