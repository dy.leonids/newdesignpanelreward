import React, { Component } from 'react';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
//import './style.css';
class ProfileDashboard extends Component {
  constructor(props){
    super(props)
    this.state={
    };
  }

  componentDidMount(){
    document.title = "Panelreward-DemoGraphics";
    sessionStorage.removeItem('categoriesData');
  }

  catView(data){
  this.props.catView(data);
  }

  render(){

    //store category data in season
    if(this.props.profileCategory.categoryData && this.props.profileCategory.categoryData.length){
      let category = this.props.profileCategory.categoryData;
      localStorage.setItem("categoriesData", JSON.stringify(category));
    }

    let categoryList="";
    let categoryItem = "";
    if(this.props.profileCategory.categoryData && this.props.profileCategory.categoryData.length){
      let categories = this.props.profileCategory.categoryData;
      categoryList = Object.keys(categories).map((item, index) => {
      categoryItem = categories[item];
        return   <div   key={index}>
                    <button className = "catBox" onClick={this.catView.bind(this, categoryItem)}>
                    {categoryItem.catName}<br/><br/>
                    <CircularProgressbar className = "progressbar" percentage={categoryItem.percentage} text={`${categoryItem.percentage}%`} />
                    <p>Update {categoryItem.catName} .</p>
                    </button>
                 </div>
      });
    }

    return (
      <div className = "dashboardback">
       <div className = "whiteback">
        
         <div className = "catheading"> <h1>Demo Graphics Dashboard</h1></div>
          
          <div className = "containerform">
          {categoryList}
          </div>

          <div className="loading">{
               this.props.requestStarted?
               <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
               :
               false
             }</div>

          </div>
            
        
        </div>
    );
  }
}
export default ProfileDashboard;
