import React, { Component } from 'react';
//import './style.css';
import { fetchEduQuestions, saveQuestionAns } from '../../actions/profileDashboard';
import CheckboxOrRadioGroup from '../CheckBoxOrRadio/CheckboxOrRadioGroup';
import LoadingDots from '../../components/Loadingdot/Loadingdots';
import { connect } from 'react-redux';

import Select from 'react-select';


class Education extends Component {

  constructor(props) {
    

    super(props)
    this.state = {
      option: [],
      selectedOption: [],
      selectoption:"",
      categoryName:"",
      carAns:"",
      mobAns:"",
      bikeAns:"",
      lapAns: "",
      currentCategoryID: '',
      currentOrderID :'',
      TotalCategory: '',
      TotalCategoryData:[]
    };
   
  }

  componentDidMount(){
    
    // let retriveCategiry = localStorage.getItem("categoriesData");
    // let categoryData = JSON.parse(retriveCategiry);
    //let  categoryData.find(item => item.cat_id == catID)
    
    //alert(categoryData.length);
   // let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
   // console.log(categoryData.find(item => item.cat_id == '5'));
      
    document.title = "Panelreward-"+ (window.location.pathname.split("/")[3].split("catName:")[1]).replace("%20", " ").replace("%20", " ");
  }
 
  componentWillMount() {
    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let catNam = (window.location.pathname.split("/")[3].split("catName:")[1]);
    var catN = catNam.replace("%20", " ");
    var catName = catN.replace("%20", " ");
    let retriveCategiry = localStorage.getItem("categoriesData");
    let categoryData = JSON.parse(retriveCategiry);
    let currentOrderNo = categoryData.find(item => item.cat_id == catID).order_no;
    this.setState({
      TotalCategory : categoryData.length,
      TotalCategoryData : categoryData,
      categoryName: catName,
      currentCategoryID : catID,
      currentOrderID: currentOrderNo
    });
    
    let userId = sessionStorage.getItem("user_id");

    if (userId && catID) {
      this.props.dispatch(fetchEduQuestions(userId, catID));
    }
  }

  componentWillReceiveProps(nextProps) {
    
    if(nextProps.educationData){
      for(var eduLoop = 0; eduLoop<nextProps.educationData.length; eduLoop++ ){
        // car 
        if(nextProps.educationData[eduLoop]._id === "5bd19a759a7ee00f7064d179"){
          this.setState({carAns : nextProps.educationData[eduLoop].userOptions });
        }
       //mobile  
       if(nextProps.educationData[eduLoop]._id === "5bd19f359a7ee00f7064d1bb"){
        this.setState({mobAns : nextProps.educationData[eduLoop].userOptions });
      }
      //bike 
      if(nextProps.educationData[eduLoop]._id === "5bd19c289a7ee00f7064d190"){
        this.setState({bikeAns : nextProps.educationData[eduLoop].userOptions });
      }
      //laptop
      if(nextProps.educationData[eduLoop]._id === "5bd19de99a7ee00f7064d1a5"){
        this.setState({lapAns : nextProps.educationData[eduLoop].userOptions });
      }
      }
    }
  }

  ShowNextPage(){
    
    let nextcat = parseInt(this.state.currentOrderID) + 1;
    let data = this.state.TotalCategoryData.find(item => item.order_no == nextcat);
    //this.props.categoryView(data.cat_id, data.catName);
    this.props.history.push("/education" +"/cat:"+data.cat_id + "/catName:" +data.catName);
     window.location.reload();
  }

  ShowFineshPage(){
    this.props.history.push('/profileDashboard');
  }

  onChange(qData, e) {
    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");
    if (userId && catID) {
      if(qData.sec_sub_ques_id){

        let quesData = {};
        quesData.sec_sub_ques_id = qData.sec_sub_ques_id;
        quesData.sub_ques_id = qData.sub_ques_id;
        quesData.ques_id = qData.ques_id;
        quesData.option_id = e.target.value;
        this.setState({ selectoption: e.target.value });
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        //this.props.dispatch(fetchEduQuestions(userId, catID));

      }else if(qData.sub_ques_id){

        let quesData = {};
        quesData.sub_ques_id = qData.sub_ques_id;
        quesData.ques_id = qData.ques_id;
        quesData.option_id = e.target.value;
        this.setState({ selectoption: e.target.value });
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
        //this.props.dispatch(fetchEduQuestions(userId, catID));

      }else{
        let quesData = {};
      quesData.ques_id = qData.ques_id;
      quesData.option_id = e.target.value;
      this.setState({ selectoption: e.target.value });
      this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      //this.props.dispatch(fetchEduQuestions(userId, catID));
      }
    }
  }

//..........................Handle change........................//

  handleChangeq12s2 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");

    this.setState({ selectedOption });
    let ansArray = [];
    if(selectedOption.length>0){
      for(var i = 0; i < selectedOption.length; i++){
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
        quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
     
    }else{
      let quesData = {};
        quesData.sub_ques_id =  "2";
        quesData.ques_id = "12";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }
  //car questions handle
  handleChangeq15s2 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");

    if(selectedOption.length <= parseInt(this.state.carAns)){
    this.setState({ selectedOption });
    let ansArray = [];
      if(selectedOption.length>0){
        for(var i = 0; i < selectedOption.length; i++){
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
          quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
          quesData.ques_id = selectedOption[0].ques.ques_id;
          quesData.optionArr = ansArray;
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      
      }else{
        let quesData = {};
          quesData.sub_ques_id =  "2";
          quesData.ques_id = "15";
          quesData.optionArr = [];
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq18s2 = (selectedOption) => {   //mobile

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");

    if(selectedOption.length <= parseInt(this.state.mobAns)){
      this.setState({ selectedOption });
      let ansArray = [];
      if(selectedOption.length>0){
        for(var i = 0; i < selectedOption.length; i++){
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
          quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
          quesData.ques_id = selectedOption[0].ques.ques_id;
          quesData.optionArr = ansArray;
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      
      }else{
        let quesData = {};
          quesData.sub_ques_id =  "2";
          quesData.ques_id = "18";
          quesData.optionArr = [];
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq16s2 = (selectedOption) => { //bike 

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");

    if(selectedOption.length <= parseInt(this.state.bikeAns)){
      this.setState({ selectedOption });
      let ansArray = [];
      if(selectedOption.length>0){
        for(var i = 0; i < selectedOption.length; i++){
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
        quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      
      }else{
        let quesData = {};
        quesData.sub_ques_id =  "2";
        quesData.ques_id = "16";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq17s2 = (selectedOption) => { //laptop

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");

    if(selectedOption.length <= parseInt(this.state.lapAns)){
      this.setState({ selectedOption });
      let ansArray = [];
      if(selectedOption.length>0){
        for(var i = 0; i < selectedOption.length; i++){
          ansArray.push(selectedOption[i].value);
        }

        let quesData = {};
          quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
          quesData.ques_id = selectedOption[0].ques.ques_id;
          quesData.optionArr = ansArray;
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      
      }else{
        let quesData = {};
          quesData.sub_ques_id =  "2";
          quesData.ques_id = "17";
          quesData.optionArr = [];
          quesData.ansType = "checkBox";
          this.props.dispatch(saveQuestionAns(userId, catID, quesData));
      }
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq42s1 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if(selectedOption.length>0){
      for(var i = 0; i < selectedOption.length; i++){
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
        quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
     
    }else{
      let quesData = {};
        quesData.sub_ques_id =  "1";
        quesData.ques_id = "42";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq41s1 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if(selectedOption.length>0){
      for(var i = 0; i < selectedOption.length; i++){
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
        quesData.sub_ques_id =  selectedOption[0].ques.sub_ques_id;
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
     
    }else{
      let quesData = {};
        quesData.sub_ques_id =  "1";
        quesData.ques_id = "41";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Option selected:`, selectedOption);
  }

  handleChangeq27 = (selectedOption) => {

    let catID = (window.location.pathname.split("/")[2].split("cat:")[1]);
    let userId = sessionStorage.getItem("user_id");


    this.setState({ selectedOption });
    let ansArray = [];
    if(selectedOption.length>0){
      for(var i = 0; i < selectedOption.length; i++){
        ansArray.push(selectedOption[i].value);
      }

      let quesData = {};
       
        quesData.ques_id = selectedOption[0].ques.ques_id;
        quesData.optionArr = ansArray;
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
     
    }else{
      let quesData = {};
        
        quesData.ques_id = "27";
        quesData.optionArr = [];
        quesData.ansType = "checkBox";
        this.props.dispatch(saveQuestionAns(userId, catID, quesData));
    }
    //console.log(`Opti
  }

  render() {
    
    const { selectedOption } = this.state;
    let eduList = "";
    let eduItem = ""
    if (this.props.success) {
      if (this.props.educationData && this.props.educationData.length) {
        let questions = this.props.educationData;
        eduList = Object.keys(questions).map((item, index) => {
          eduItem = questions[item];

          if(eduItem.selectionType === "select"){

          return <div key={index} className="eduQForm">
            <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4></div>

              <select name={index} value={this.state.option[index]} onChange={this.onChange.bind(this, eduItem)}>
              <option> --------- </option>
              {
                eduItem.options.map((optionItem, i) => {

                  //eduItem
                  if (eduItem.userOptions == 0) {
                    return <option key={i} value={optionItem.option_id}   > {optionItem.options} </option>

                  }
                  else {
                    if (eduItem.userOptions == optionItem.option_id) {
                      return <option selected key={i} value={optionItem.option_id}   > {optionItem.options} </option>
                    }
                    else {
                      return <option key={i} value={optionItem.option_id}   > {optionItem.options} </option>
                    }
                  }
                })
              }
             </select>
            </div>
          }else if(eduItem.selectionType === "checkBox"){

            var employees = {
              accounting: []
          };
          var employees1 = {
            accounting1: []
        };


          
          if(eduItem.userOptions){

          
            if(eduItem.userOptions.length>0)
            {

              for(var userOpt = 0; userOpt < eduItem.options.length; userOpt++)
              {
                    var item = eduItem.options[userOpt];   
        
                    if(!eduItem.userOptions.includes(item.option_id)){
                      employees.accounting.push({ 
                        "value" : item.option_id,
                        "label"  : item.options,
                        "ques": eduItem
                    });
                  }
                  else
                  {
                    employees1.accounting1.push({ 
                      "value" : item.option_id,
                      "label"  : item.options,
                      "ques": eduItem
                  });
                    
                  }
              }
            }
            else
            {
            for(var j in eduItem.options) {  
              var item = eduItem.options[j];   
              employees.accounting.push({ 
                  "value" : item.option_id,
                  "label"  : item.options,
                  "ques": eduItem
              });
            }
            }
          }else{
            for(var j in eduItem.options) {  
              var item = eduItem.options[j];   
              employees.accounting.push({ 
                  "value" : item.option_id,
                  "label"  : item.options,
                  "ques": eduItem
              });
            }
          }

          //--------------------------checkbox options----------------------------------//

          if(eduItem._id == "5bd1968d9a7ee00f7064d15e")
          {
            return(

              <div key={index} className="eduQForm">
                <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
                </div> 
                    <Select
                      key={index}
                      value={employees1.accounting1}
                      onChange={this.handleChangeq12s2}
                      isMulti  ={true}
                      options={employees.accounting}
                    />
               </div>
            )
          }
          else if(eduItem._id == "5bd19af09a7ee00f7064d182") {

            return(

              <div key={index} className="eduQForm">
                <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
                </div>                      
                    <Select
                      key={index}
                      value={employees1.accounting1}
                      onChange={this.handleChangeq15s2}
                      isMulti  ={true}
                      options={employees.accounting}
                    />
               </div>
            )
         }
         //mobile
         else if(eduItem._id == "5bd19fc89a7ee00f7064d1c3") {

          return(

            <div key={index} className="eduQForm">
              <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
              </div>                      
                  <Select
                    key={index}
                    value={employees1.accounting1}
                    onChange={this.handleChangeq18s2}
                    isMulti  ={true}
                    options={employees.accounting}
                  />
             </div>
          )
       }
       //bike
       else if(eduItem._id == "5bd19cc89a7ee00f7064d199") {

        return(

          <div key={index} className="eduQForm">
            <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
            </div>                      
                <Select
                  key={index}
                  value={employees1.accounting1}
                  onChange={this.handleChangeq16s2}
                  isMulti  ={true}
                  options={employees.accounting}
                />
           </div>
        )
     }
     else if(eduItem._id == "5bd19e4f9a7ee00f7064d1ae") { //laptop

      return(

        <div key={index} className="eduQForm">
          <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
          </div>                      
              <Select
                key={index}
                value={employees1.accounting1}
                onChange={this.handleChangeq17s2}
                isMulti  ={true}
                options={employees.accounting}
              />
         </div>
      )
   }
   else if(eduItem._id == "5bd17d049a7ee00f7064d080") {

    return(

      <div key={index} className="eduQForm">
        <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
        </div>                      
            <Select
              key={index}
              value={employees1.accounting1}
              onChange={this.handleChangeq42s1}
              isMulti  ={true}
              options={employees.accounting}
            />
       </div>
    )
 }
 else if(eduItem._id == "5bd17be29a7ee00f7064d062") {

  return(

    <div key={index} className="eduQForm">
      <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
      </div>                      
          <Select
            key={index}
            value={employees1.accounting1}
            onChange={this.handleChangeq41s1}
            isMulti  ={true}
            options={employees.accounting}
          />
     </div>
  )
}
else if(eduItem._id == "5bd192c29a7ee00f7064d14b") {

  return(

    <div key={index} className="eduQForm">
      <div className="eduQFormHeading"><h4 >{eduItem.questions}</h4>
      </div>                      
          <Select
            key={index}
            value={employees1.accounting1}
            onChange={this.handleChangeq27}
            isMulti  ={true}
            options={employees.accounting}
          />
     </div>
  )
}
          
          }
        });
      }
    }


    let showNextButton = ''
    if(this.props.educationData){
      if(this.state.currentOrderID < this.state.TotalCategory){
        showNextButton = <div><button className="profileDashboardBtn" onClick={this.ShowNextPage.bind(this)}>Next</button></div>
      }else{
        showNextButton = <div><button  className="profileDashboardBtn" onClick={this.ShowFineshPage.bind(this)}>Finesh</button></div>
      }
    }
    
    
    
    return (
      <div className="edubackground">
        <div className="eduwhiteback" >
          <div className="eduheading"><h1>{this.state.categoryName} Question</h1></div>

          <div className="eduContainer">

            {eduList}
            <br/>
            {showNextButton}
            <div className="loading">{
               this.props.requestStarted?
               <div className="preloader"><LoadingDots interval={100} dots={5}/></div>
               :
               false
             }</div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    educationData : state.profileDashboard.educationData,
    subQuestions : state.profileDashboard.subQuestions,
    success : state.profileDashboard.success,
    requestStarted : state.common.requestStarted
  }
}

export default connect(mapStateToProps)(Education);
