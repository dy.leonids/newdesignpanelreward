
const initialState = {
  studies: {},
  stash: {},

  message: String,
  states: {},
  success: false

}

const study = (state = initialState, action) => {
  switch (action.type) {

    case 'SET_STUDY':
      return {
        ...state,
        //messages : false,
        studies : action.studyData.studyData,
        totalStudies : action.studyData.totalStudy,
        StudyCountData : false
      }

      case 'SET_STUDY_PARTICIPATE_STATUS':
        return {
          ...state,
          messages : action.response.message,
          success : action.response.success,
          StudyCountData : false
        }

      case 'FAIL_STUDY_PARTICIPATE_STATUS':
        return{
          ...state,
          messages : action.response.error,
          success : action.response.success,
          StudyCountData : false
        } 
        
        
        case 'SET_STUDY_COUNT_DATA':
        return {   
          ...state,
          StudyCountData : action.countResponse.StudyCountData,
          success : action.countResponse.success
        }

    default:
      return state;
  }
};
export default study;
