
const initialState = {
  category: {},
  stash: {},

  message: String,
  states: {},
  success: false
}

const profileDashboard = (state = initialState, action) => {
  switch (action.type) {

    case 'SET_CATEGORY':
      return {
        ...state,
        category: action.categoryData
      }

    case 'SET_EDUCATION':
      return {
        ...state,
        educationData: action.educationData.questions,
        subQuestions: action.educationData.subQuestions,
        success: action.educationData.success
      }

    default:
      return state;
  }
};
export default profileDashboard;
