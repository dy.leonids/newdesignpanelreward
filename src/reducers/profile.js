
const initialState = {
  studies: {},
  stash: {},

  message: String,
  states: {}
}

const profile = (state = initialState, action) => {
  switch (action.type) {

    case 'SET_PROFILE_LOGOUT':
      return {
        userProfile: false,
        setUser:false
      }

    case 'SET_PROFILE':
      return {
        ...state,
        profileImageUploadRes: false,
        userProfile : action.profileData.profile,
        setUser : action.profileData.profile
      }      

    case 'SET_PROFILE_UPLOAD_IMAGE_RESPONSE':
      return {
        ...state,
        profileImageUploadRes : action.updateResponse
      }

    default:
      return state;
  }
};
export default profile;
