const initialState = {
  success: false,
  message: ""
  }
  const resetPassword = (state = initialState , action) =>{
    switch (action.type) {

      case 'SET_RESET_PASS_MESSAGE':
      return{
        ...state,
        message: action.response.message,
        success: action.response.success
      }

      case 'SET_RESET_PASS_MESSAGE_FAIL':
      return{
        ...state,
        message: false,
        success: false
      }

      default:
      return state;
    }
  };
export default resetPassword;
