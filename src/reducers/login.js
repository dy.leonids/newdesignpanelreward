import isEmpty from 'lodash/isEmpty';
const initialState = {
  error: [],
  users:{},
  success: false,
  message: ""
  }
  const login = (state = initialState , action) =>{
    switch (action.type) {

      case 'SET_AUTH_CLIENT':
          return {
          ...state,
            authClient: action.authClient,
            authenticated : !isEmpty(action.authClient),
            error: action.error,
            success: action.success,
          }

      case 'SET_LOGIN_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: action.authFailResponse.error
          }

          case 'LOGOUT' :
          return {
            ...state,
            authClient: null,
            authenticated : false
          }

          case 'SET_MESSAGE_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: null
          }

          case 'SET_RESET_PASS_MESSAGE_FAIL':
          return{
            ...state,
            authClient : null,
            authenticated :false,
            success: false,
            message: null
          }

          default:
          return state;
        }
  };
export default login;
